#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import boto3, csv, time, sys, os, psycopg2,pandas as pd, mysql.connector, re, numpy as np


# In[ ]:


s3 = boto3.resource('s3')


# In[ ]:


from datetime import datetime


# In[ ]:


from datetime import date


# In[ ]:


def run_query(client, query, database, s3_output):
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )
    # print('Execution ID: ' + response['QueryExecutionId'])
    return response


# In[ ]:


def get_query_status(client, Execution_Id):
    response = client.get_query_execution(
        QueryExecutionId=Execution_Id
    )
    query_id = response['QueryExecution']['QueryExecutionId']
    status = response['QueryExecution']['Status']['State']
    print('Started')
    print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    while status not in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
        # print('|' + status, end='')
        time.sleep(30)
        response = client.get_query_execution(
            QueryExecutionId=Execution_Id
        )
        status = response['QueryExecution']['Status']['State']
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    if status in ['SUCCEEDED']:
        return True
    if status in ['FAILED', 'CANCELLED']:
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))
        print("Failure Reason: {}".format(
            response['QueryExecution']['Status'].get('StateChangeReason', 'Error Unknown')))
        return False
    else:
        return False


# In[ ]:


def run_query_to_athena(query, output_filename):
    client = boto3.client('athena', 'us-east-1' )
    s3_output = "s3://aws-athena-query-results-190020191201-us-east-1/"
    s3_bucket="aws-athena-query-results-190020191201-us-east-1"
    database = 'express_dwh'
    res = run_query(client, query, database, s3_output)

    q_id = res['QueryExecutionId']
    if get_query_status(client, q_id):
        print("Query Successful. Downloading data: ")
        os.system("aws s3 cp " + s3_output + q_id + ".csv  {}".format(output_filename))
        s3_key = q_id + '.csv'
        local_filename = output_filename + '.csv'
        s3.Bucket(s3_bucket).download_file(s3_key, local_filename)

        return True
    else:
        print("Query Unsuccessful. Please check the query or the Athena server status")
        return False


# In[ ]:


query_act = """
SELECT cs_sl as sl,date(cs_sd + interval '330' minute) as sd_date,case when pdt in ('B2B','Heavy') then pdt else 'B2C' end as product_type,
count(wbn) as "Total",
count(case when oc=cs_sl and cs_st = 'UD' then wbn end) as "Fresh",
count(case when oc<>cs_sl and cs_st = 'UD' then wbn end) as "Mix",
count(case when cs_st in ('RT','PU') then wbn end) as "Return"
    FROM
    (   
      SELECT cs_sl,oc,cs_sd,wbn,cs_st,pdt,cs_act,
            row_number() over (partition BY wbn,pid ORDER BY action_date DESC) AS ROW
      FROM express_dwh_3m.package_scan_latest_pq_3m
      WHERE ad >= date_format((date_trunc('day',CURRENT_TIMESTAMP) - interval '60' day) - interval '00' minute, '%Y-%m-%d-%H')
      AND ad <= date_format((date_trunc('day',CURRENT_TIMESTAMP) + interval '1' day) - interval '00' minute, '%Y-%m-%d-%H')
      AND cs_sd >= (date_trunc('day',CURRENT_TIMESTAMP) - interval '53' day) - interval '330' minute
      AND cs_sd <= (date_trunc('day',CURRENT_TIMESTAMP) + interval '0' day) - interval '330' minute 
      AND cs_act in ('+B','+S')
    )
    WHERE ROW = 1
    
    group by 1,2,3
"""


# In[ ]:


dump_path = "process_ship"
result=run_query_to_athena(query_act, dump_path)


# In[ ]:


local_filename= 'process_ship' + '.csv'
output_act=pd.read_csv(local_filename)


# In[ ]:


output_act.head()


# In[ ]:


from sqlalchemy import create_engine


# In[ ]:


import sqlalchemy


# In[ ]:


engine = create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                              format("shree", "snop321#",
                                                     "datawarehouse-lastmile-analytics-rds.ceypiyhweprx.us-east-1.rds.amazonaws.com", "SNOP"))


# In[ ]:


cnx = engine.raw_connection()


# In[ ]:


output_act.to_sql(name='processed_shipments', con=engine, chunksize=1000, if_exists = 'append', index=False,  dtype={'sd_date': sqlalchemy.DateTime()})


# In[ ]:


cnx.close()

