import boto3, csv, time, sys, os, psycopg2,pandas as pd, mysql.connector, re, numpy as np
s3 = boto3.resource('s3')
from datetime import datetime
from datetime import date
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from google.auth.transport.requests import Request
import pandas as pd
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
SPREADSHEET_ID = "1OkXhkej3r7PEOj0QpLobt4B0XmnoL-Fk2n1a1IokDU0"

def pull_sheet_data(SCOPES,Id,Rn):
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('/srv/shiny-server/snop/scripts/token3.pickle'):
        with open('/srv/shiny-server/snop/scripts/token3.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                os.path.join("/srv/shiny-server/snop", "scripts", "oauth_snop1.json"), SCOPES)
            
            creds = flow.run_local_server(port=8080)
        # Save the credentials for the next run
        with open('/srv/shiny-server/snop/scripts/token3.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=Id,
                                range=Rn).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        rows = sheet.values().get(spreadsheetId=Id,
                                  range=Rn).execute()
        data = rows.get('values')
        print("COMPLETE: Data copied")
        df = pd.DataFrame(data)
        df.columns = df.iloc[0]
        df = df.drop(df.index[0])
        return df


def run_query(client, query, database, s3_output):
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )
    # print('Execution ID: ' + response['QueryExecutionId'])
    return response

def get_query_status(client, Execution_Id):
    response = client.get_query_execution(
        QueryExecutionId=Execution_Id
    )
    query_id = response['QueryExecution']['QueryExecutionId']
    status = response['QueryExecution']['Status']['State']
    print('Started')
    print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    while status not in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
        # print('|' + status, end='')
        time.sleep(30)
        response = client.get_query_execution(
            QueryExecutionId=Execution_Id
        )
        status = response['QueryExecution']['Status']['State']
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    if status in ['SUCCEEDED']:
        return True
    if status in ['FAILED', 'CANCELLED']:
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))
        print("Failure Reason: {}".format(
            response['QueryExecution']['Status'].get('StateChangeReason', 'Error Unknown')))
        return False
    else:
        return False

    
def run_query_to_athena(query, output_filename):
    client = boto3.client('athena', 'us-east-1' )
    s3_output = "s3://aws-athena-query-results-190020191201-us-east-1/"
    s3_bucket="aws-athena-query-results-190020191201-us-east-1"
    database = 'express_dwh'
    res = run_query(client, query, database, s3_output)

    q_id = res['QueryExecutionId']
    if get_query_status(client, q_id):
        print("Query Successful. Downloading data: ")
        os.system("aws s3 cp " + s3_output + q_id + ".csv  {}".format(output_filename))
        s3_key = q_id + '.csv'
        local_filename = output_filename + '.csv'
        s3.Bucket(s3_bucket).download_file(s3_key, local_filename)

        return True
    else:
        print("Query Unsuccessful. Please check the query or the Athena server status")
        return False

   
    
def get_results_as_df(query,filename):
   result = run_query_to_athena(query,filename)
   output = pd.read_csv(filename+'.csv')
   return output


df_lma = pull_sheet_data(SCOPES,SPREADSHEET_ID , "LMA_current!A1:E")
df_lma['new_name1'] = df_lma['new_name'].str.lower()
df_lma['current_LMA'] = pd.to_numeric(df_lma['current_LMA'])
df_lma['current_stores'] = pd.to_numeric(df_lma['current_stores'])
df_lma['target_LMA'] = pd.to_numeric(df_lma['target_LMA'])
df_lma['target_stores'] = pd.to_numeric(df_lma['target_stores'])
df_lma.head()


query_disp = """
SELECT dpd_date,lower(cn) as new_name1,
sum(case when Mode = 'FE' then wbn_count end) as FE_disp,
sum(case when Mode = 'ADM' then wbn_count end) as ADM_disp,
sum(case when Mode = 'Adhoc' then wbn_count end) as Adhoc_disp,
sum(case when Mode = 'Onroll' then wbn_count end) as Onroll_disp,
sum(case when Mode = 'Other' then wbn_count end) as Other_disp,
count(distinct (case when Mode = 'FE' then concat(cast(dpd_date as varchar),fu_emp_id) end)) as FE_mandays
FROM
    (
        SELECT date(dpd + interval '330' minute) as dpd_date,cn,wbn_count,fu_emp_id,
        case when lower(md) = 'regular' and length(fu_emp_id) = 6 then 'FE'
        when lower(md) in ('agent','gig') then 'ADM'
        when lower(md) = 'adhoc' then 'Adhoc'
        when fu_emp_id like 'SSN%' then 'Onroll'

        else 'Others' end as "Mode"
        FROM
            (
                SELECT dpd,cn,wbn_count,md,fu_emp_id
                    ,row_number() over (partition BY dwbn ORDER BY action_date DESC) AS ROW
                FROM express_dwh.dispatch_lm_s3_parquet
                 WHERE ad >= date_format((date_trunc('day',CURRENT_DATE) - interval '12' DAY) - interval '00' MINUTE, '%Y-%m-%d-%H')
               AND ad <= date_format((date_trunc('day',CURRENT_DATE) - interval '0' DAY) - interval '00' MINUTE, '%Y-%m-%d-%H')
               AND dpd >= (date_trunc('day',CURRENT_TIMESTAMP) - interval '8' DAY) - interval '330' MINUTE
               AND dpd <= (date_trunc('day',CURRENT_TIMESTAMP) - interval '1' DAY) - interval '330' MINUTE

            )
        WHERE ROW = 1
    )
group by 1,2
"""

df_disp = get_results_as_df(query_disp,"disp")
df_disp.head()

df_disp = df_disp.fillna(0)

df_adm_disp = df_disp.pivot(index = 'new_name1', columns = 'dpd_date')['ADM_disp']
df_adm_disp = df_adm_disp.fillna(40)

df_test = pd.merge(df_lma,df_adm_disp,on='new_name1',how='left')

df_test.columns = ['new_name','target_lma','target_stores','current_lma','current_stores','T8','T7','T6','T5','T4','T3','T2','T1']
from sqlalchemy import create_engine
import sqlalchemy

engine = create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                              format("shree", "snop321#",
                                                     "datawarehouse-lastmile-analytics-rds.ceypiyhweprx.us-east-1.rds.amazonaws.com", "SNOP"))
with engine.connect() as connection:
    result = connection.execute("delete from lma_target_current_new")

cnx = engine.raw_connection()
df_test.to_sql(name='lma_target_current_new', con=engine, chunksize=1000, if_exists = 'append', index=False)
cnx.close()



