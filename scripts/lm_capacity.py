import boto3, csv, time, sys, os, psycopg2,pandas as pd, mysql.connector, re, numpy as np
s3 = boto3.resource('s3')
from datetime import datetime
from datetime import date
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from google.auth.transport.requests import Request
import pandas as pd
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
SPREADSHEET_ID = "1OkXhkej3r7PEOj0QpLobt4B0XmnoL-Fk2n1a1IokDU0"

def pull_sheet_data(SCOPES,Id,Rn):
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token3.pickle'):
        with open('token3.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                r"C:\Users\delhivery\Documents\scripts\oauth_snop2.json", SCOPES)
            
            creds = flow.run_local_server(port=8080)
        # Save the credentials for the next run
        with open('token3.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=Id,
                                range=Rn).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        rows = sheet.values().get(spreadsheetId=Id,
                                  range=Rn).execute()
        data = rows.get('values')
        print("COMPLETE: Data copied")
        df = pd.DataFrame(data)
        df.columns = df.iloc[0]
        df = df.drop(df.index[0])
        return df


def run_query(client, query, database, s3_output):
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )
    # print('Execution ID: ' + response['QueryExecutionId'])
    return response

def get_query_status(client, Execution_Id):
    response = client.get_query_execution(
        QueryExecutionId=Execution_Id
    )
    query_id = response['QueryExecution']['QueryExecutionId']
    status = response['QueryExecution']['Status']['State']
    print('Started')
    print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    while status not in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
        # print('|' + status, end='')
        time.sleep(30)
        response = client.get_query_execution(
            QueryExecutionId=Execution_Id
        )
        status = response['QueryExecution']['Status']['State']
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    if status in ['SUCCEEDED']:
        return True
    if status in ['FAILED', 'CANCELLED']:
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))
        print("Failure Reason: {}".format(
            response['QueryExecution']['Status'].get('StateChangeReason', 'Error Unknown')))
        return False
    else:
        return False

    
def run_query_to_athena(query, output_filename):
    client = boto3.client('athena', 'us-east-1' )
    s3_output = "s3://aws-athena-query-results-190020191201-us-east-1/"
    s3_bucket="aws-athena-query-results-190020191201-us-east-1"
    database = 'express_dwh'
    res = run_query(client, query, database, s3_output)

    q_id = res['QueryExecutionId']
    if get_query_status(client, q_id):
        print("Query Successful. Downloading data: ")
        os.system("aws s3 cp " + s3_output + q_id + ".csv  {}".format(output_filename))
        s3_key = q_id + '.csv'
        local_filename = output_filename + '.csv'
        s3.Bucket(s3_bucket).download_file(s3_key, local_filename)

        return True
    else:
        print("Query Unsuccessful. Please check the query or the Athena server status")
        return False

   
    
def get_results_as_df(query,filename):
   result = run_query_to_athena(query,filename)
   output = pd.read_csv(filename+'.csv')
   return output

query_disp = """ 
SELECT dpd_date,lower(cn) as center1,
sum(case when Mode = 'FE' then wbn_count end) as FE_disp,
sum(case when Mode = 'ADM' then wbn_count end) as ADM_disp,
sum(case when Mode = 'Adhoc' then wbn_count end) as Adhoc_disp,
sum(case when Mode = 'Onroll' then wbn_count end) as Onroll_disp,
sum(case when Mode = 'Other' then wbn_count end) as Other_disp,
count(distinct (case when Mode = 'FE' then concat(cast(dpd_date as varchar),fu_emp_id) end)) as FE_mandays
FROM
    (
        SELECT date(dpd + interval '330' minute) as dpd_date,cn,wbn_count,fu_emp_id,
        case when lower(md) = 'regular' and length(fu_emp_id) = 6 then 'FE' 
        when lower(md) in ('agent','gig') then 'ADM'
        when lower(md) = 'adhoc' then 'Adhoc'
        when fu_emp_id like 'SSN%' then 'Onroll'
        
        else 'Others' end as "Mode"
        FROM
            (
                SELECT dpd,cn,wbn_count,md,fu_emp_id
                    ,row_number() over (partition BY dwbn ORDER BY action_date DESC) AS ROW
                FROM express_dwh.dispatch_lm_s3_parquet
                 WHERE ad >= date_format((date_trunc('day',CURRENT_DATE) - interval '12' DAY) - interval '00' MINUTE, '%Y-%m-%d-%H')
               AND ad <= date_format((date_trunc('day',CURRENT_DATE) - interval '0' DAY) - interval '00' MINUTE, '%Y-%m-%d-%H')
               AND dpd >= (date_trunc('day',CURRENT_TIMESTAMP) - interval '8' DAY) - interval '330' MINUTE
               AND dpd <= (date_trunc('day',CURRENT_TIMESTAMP) - interval '1' DAY) - interval '330' MINUTE

            )
        WHERE ROW = 1
    )
group by 1,2
"""
query_vol = """select center1,sum(vol)/nullif(count(distinct concat(center1,cpd_day)),0) as vol from (
SELECT lower(cn) as center1,date_format(date(date_cpd + interval '330' minute),'%W') as cpd_day,count(wbn) as vol
    FROM
    (   
      SELECT cn,wbn,date_cpd,
            row_number() over (partition BY wbn ORDER BY action_date DESC) AS ROW
      FROM express_dwh_3m.package_s3_parquet_3m
      WHERE ad >= date_format((date_trunc('day',CURRENT_DATE) - interval '10' DAY) - interval '00' MINUTE, '%Y-%m-%d-%H')
               AND ad <= date_format((date_trunc('day',CURRENT_DATE) - interval '0' DAY) - interval '00' MINUTE, '%Y-%m-%d-%H')
               AND date_cpd >= (date_trunc('day',CURRENT_TIMESTAMP) - interval '5' DAY) - interval '330' MINUTE
               AND date_cpd <= (date_trunc('day',CURRENT_TIMESTAMP) - interval '1' DAY) - interval '330' MINUTE
      AND pdt not in ('B2B','Heavy') 
    )
    WHERE ROW = 1
    group by 1,2) where cpd_day not in ('Sunday')
    group by 1"""

query_onroll = """
with facility_data as 
(SELECT id,
          cn,
          cn_type,
          cn||' ('||STATE||')' AS hq,
                            region
  FROM
     (SELECT "property_facility_facility_code" AS id ,
             property_zone AS region,
             "property_active" ,
             property_country ,
             "property_facility_name" AS cn,
             "property_facility_facility_type" AS cn_type,
             property_city AS city,
             property_state AS STATE,
             row_number() over (partition BY "property_facility_facility_code"
                                ORDER BY "action_date" DESC) AS ROW
      FROM express_dwh.facility_ad_json)
  WHERE ROW=1
     AND upper(cn_type) IN ('DC',
                            'DC,PC')
     AND NOT upper(cn) LIKE '%DPP%'
     AND NOT upper(cn) LIKE '%3PL%'
     AND upper(property_country) = 'INDIA'
     AND NOT upper(cn) LIKE 'TEST%' -- and cn ='' --ENTER THE FACILITY NAME
 )

SELECT lower(a.hq) AS center1,
      count(CASE WHEN designation = 'Field Supervisor' THEN Employee_id END) AS "FS",
      count(CASE WHEN designation NOT IN ('Field Supervisor') THEN Employee_id END) AS "TL_ATL"
FROM
  facility_data a
LEFT JOIN
  (
  SELECT darwin_facility_id,
          Employee_id,
          designation
   FROM
     ( SELECT Employee_id,
              darwin_facility_id,
              is_active,
              dojdt,
              designation ,
              row_number() OVER (partition BY User_uuid
                                 ORDER BY action_date DESC) ROW
      FROM ums_user_parquet)
   WHERE ROW = 1
     AND Employee_id IS NOT NULL
     AND Employee_id != ''
     AND designation IN ('Team Lead',
                         'Field Supervisor',
                         'Assistant Team Lead')
     AND is_active = TRUE
     AND date(dojdt) <= (date_trunc('day',CURRENT_TIMESTAMP) - interval '10' DAY) - interval '330' MINUTE
     and darwin_facility_id in (select id from facility_data)
     ) b 
     ON a.id = b.darwin_facility_id
GROUP BY 1"""

df_lma = pull_sheet_data(SCOPES,SPREADSHEET_ID , "LMA_current!A1:E")
df_lma['center1'] = df_lma['new_name'].str.lower()
df_lma['current_LMA'] = pd.to_numeric(df_lma['current_LMA'])
df_lma['current_stores'] = pd.to_numeric(df_lma['current_stores'])
df_lma['target_LMA'] = pd.to_numeric(df_lma['target_LMA'])
df_lma['target_stores'] = pd.to_numeric(df_lma['target_stores'])
df_lma.head()
df_lma['center1'] = df_lma['center1'].str.split(' \(').str[0]

df_perc = pull_sheet_data(SCOPES, SPREADSHEET_ID , "del_clo_perc!A1:C")
df_perc['center1'] = df_perc['new_name'].str.lower()
df_perc['del_perc'] = pd.to_numeric(df_perc['del_perc'])
df_perc['clo_perc'] = pd.to_numeric(df_perc['clo_perc'])
df_perc.head()
df_perc['center1'] = df_perc['center1'].str.split(' \(').str[0]

df_reloc = pull_sheet_data(SCOPES, SPREADSHEET_ID , "relocation!A1:B")
df_reloc['center1'] = df_reloc['old_name'].str.lower()
df_reloc['cn_new'] = df_reloc['new_name'].str.lower()
df_reloc.head()
df_reloc['center1'] = df_reloc['center1'].str.split(' \(').str[0]
df_reloc['cn_new'] = df_reloc['cn_new'].str.split(' \(').str[0]

df_master = pull_sheet_data(SCOPES, SPREADSHEET_ID , "center_master!A1:F")
df_master['center1'] = df_master['new_name'].str.lower()
df_master.head()
df_master['center1'] = df_master['center1'].str.split(' \(').str[0]

df_approved = pull_sheet_data(SCOPES, SPREADSHEET_ID , "approved_count!A1:B")
df_approved['center1'] = df_approved['new_name'].str.lower()
df_approved['count1'] = pd.to_numeric(df_approved['count1'])
df_approved.head()
df_approved['center1'] = df_approved['center1'].str.split(' \(').str[0]
df_approved = df_approved[['center1','count1']]
df_approved = df_approved.groupby('center1').agg({'count1': 'sum'}).reset_index()


df_active = pull_sheet_data(SCOPES, SPREADSHEET_ID , "active_count!A1:B")
df_active['center1'] = df_active['new_name'].str.lower()
df_active['count1'] = pd.to_numeric(df_active['count1'])
df_active.head()
df_active['center1'] = df_active['center1'].str.split(' \(').str[0]
df_active = pd.merge(df_active,df_reloc,on='center1',how='left')
df_active['cn_new'] = df_active['cn_new'].fillna(df_active['center1'])
df_active['cn_new'] = df_active['cn_new'].str.lower()
df_active = df_active[['cn_new','count1']]
df_active = df_active.rename(columns={'cn_new':'center1'})
df_active = df_active.groupby('center1').agg({'count1': 'sum'}).reset_index()


df_disp = get_results_as_df(query_disp,"disp")
df_disp.head()
df_disp['center1'] = df_disp['center1'].str.split(' \(').str[0]
df_disp = pd.merge(df_disp,df_reloc,on='center1',how='left')
df_disp['cn_new'] = df_disp['cn_new'].fillna(df_disp['center1'])
df_disp['cn_new'] = df_disp['cn_new'].str.lower()
df_disp = df_disp[['dpd_date','cn_new','FE_disp','ADM_disp','Adhoc_disp','Onroll_disp','Other_disp','FE_mandays']]
df_disp = df_disp.rename(columns={'cn_new':'center1'})
df_disp = df_disp.fillna(0)
df_disp = df_disp.groupby(['dpd_date','center1']).agg({'FE_disp': 'sum','ADM_disp': 'sum','Adhoc_disp': 'sum','Onroll_disp': 'sum','Other_disp': 'sum','FE_mandays': 'sum'}).reset_index()

df_vol = get_results_as_df(query_vol,"vol")
df_vol.head()
df_vol['center1'] = df_vol['center1'].str.split(' \(').str[0]
df_vol = pd.merge(df_vol,df_reloc,on='center1',how='left')
df_vol['cn_new'] = df_vol['cn_new'].fillna(df_vol['center1'])
df_vol['cn_new'] = df_vol['cn_new'].str.lower()
df_vol = df_vol[['cn_new','vol']]
df_vol = df_vol.rename(columns={'cn_new':'center1'})
df_vol = df_vol.fillna(0)
df_vol = df_vol.groupby('center1').agg({'vol': 'sum'}).reset_index()

df_onroll = get_results_as_df(query_onroll,"onroll")
df_onroll.head()
df_onroll['center1'] = df_onroll['center1'].str.split(' \(').str[0]
df_onroll = pd.merge(df_onroll,df_reloc,on='center1',how='left')
df_onroll['cn_new'] = df_onroll['cn_new'].fillna(df_onroll['center1'])
df_onroll['cn_new'] = df_onroll['cn_new'].str.lower()
df_onroll = df_onroll[['cn_new','FS','TL_ATL']]
df_onroll = df_onroll.rename(columns={'cn_new':'center1'})
df_onroll = df_onroll.groupby('center1').agg({'FS': 'sum','TL_ATL': 'sum'}).reset_index()

df_fe_disp = df_disp.assign(FE_disp_cap = df_disp['FE_disp']/df_disp['FE_mandays'])
df_fe_disp = df_fe_disp.pivot(index = 'center1', columns = 'dpd_date')['FE_disp_cap']
df_fe_disp = df_fe_disp.fillna(40)

df_fe_disp = df_fe_disp.sort_values(by='aalo_genhospital_d', ascending=False,axis = 1)
df_fe_disp = df_fe_disp.assign(max3=df_fe_disp.values[:,2])


df_fe_disp.index.name = 'center1'
df_fe_disp.reset_index(inplace=True)
df_fe_disp = df_fe_disp[['center1','max3']]

df_test = pd.merge(df_master,df_fe_disp,on='center1',how='left')

df_test['max3_new'] = df_test.apply(lambda X: min(max(X.max3,50),90) if X.tier1 == 'M' else min(max(X.max3,45),80) if X.tier1 == 'T1' else min(max(X.max3,40),70) if X.tier1 == 'T2' else min(max(X.max3,40),60), axis = 1)

df_test = pd.merge(df_test,df_vol,on='center1',how='left')
df_test = pd.merge(df_test,df_active[['center1','count1']],on='center1',how='left')
# df_test['center'] = df_test['center'].str.lower()
df_test = pd.merge(df_test,df_onroll[['center1','FS','TL_ATL']],on='center1',how='left')
df_test = df_test.fillna(0)
df_lma['current_adm'] = df_lma['current_LMA']+df_lma['current_stores']
df_lma['target_adm'] = df_lma['target_LMA']+df_lma['target_stores']
df_test = pd.merge(df_test,df_lma[['center1','current_adm','target_adm']],on='center1',how='left')
df_test = df_test.fillna(0)
df_test['overall_current_capacity'] = ((df_test['count1']+(df_test['FS']/2))*df_test['max3_new']*0.85) + df_test['current_adm']
df_test = pd.merge(df_test,df_perc[['center1','del_perc','clo_perc']],on='center1',how='left')
df_test['del_perc'] = df_test['del_perc'].fillna(0.9)
df_test['clo_perc'] = df_test['clo_perc'].fillna(0.7)
df_test['capacity_required'] = df_test['vol']*(df_test['del_perc']/df_test['clo_perc'])
df_test['capacity_util'] = df_test['capacity_required']/df_test['overall_current_capacity']
df_test['add_capacity_required'] = df_test.apply(lambda X: max(X.capacity_required-X.overall_current_capacity,0),axis=1)
df_test['ADM_ramp_up'] = df_test.apply(lambda X: max(X.target_adm-X.current_adm,0),axis=1)

df_test = pd.merge(df_test,df_approved[['center1','count1']],on='center1',how='left')
df_test = df_test.fillna(0)


df_test1 = df_test[['center1','city','region','state','tier1','max3_new','vol','count1_x','current_adm','target_adm','overall_current_capacity','del_perc','clo_perc','capacity_required','capacity_util','add_capacity_required','ADM_ramp_up','count1_y','FS','TL_ATL']]
# df_test1.columns = ['new_name1','new_name1_x','city','region','state','tier1','max3_new','vol','count1_x','current_adm','target_adm','overall_current_capacity','del_perc','clo_perc','capacity_required','capacity_util','add_capacity_required','ADM_ramp_up','count1_y','FS','TL_ATL']
# df_test1 = df_test1[['new_name1','city','region','state','tier1','max3_new','vol','count1_x','current_adm','target_adm','overall_current_capacity','del_perc','clo_perc','capacity_required','capacity_util','add_capacity_required','ADM_ramp_up','count1_y','FS','TL_ATL']]
df_test1 = df_test1.rename(columns={'center1':'new_name','max3_new': 'FE_disp_cap','count1_x':'FE_active_count','count1_y':'FE_approved_count'})

df_test1 = df_test1.replace([np.inf, -np.inf], np.nan)
df_test1 = df_test1.fillna(0)

df_test1.to_json('lm_capacity_final.json', orient='records', lines=True)

s3 = boto3.resource('s3')
s3.meta.client.upload_file('lm_capacity_final.json', 'data-lake-temp-tables', 'SNOP/final_data/lm_capacity_final.json')


from sqlalchemy import create_engine
import sqlalchemy

engine = create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                              format("shree", "snop321#",
                                                     "datawarehouse-lastmile-analytics-rds.ceypiyhweprx.us-east-1.rds.amazonaws.com", "SNOP"))

with engine.connect() as connection:
    result = connection.execute("delete from lm_capacity_new")

cnx = engine.raw_connection()
df_test1.to_sql(name='lm_capacity_new', con=engine, chunksize=1000, if_exists = 'append', index=False)
cnx.close()


# CREATE EXTERNAL TABLE SNOP_lm_capacity_final (
#   new_name string,city string,region string,state string,tier1 string,FE_disp_cap double,vol double,FE_active_count double,current_adm double,target_adm double,overall_current_capacity double,del_perc double,clo_perc double,capacity_required double,capacity_util double,add_capacity_required double ,ADM_ramp_up double,FE_approved_count double,FS double,TL_ATL double
#   )           
# ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
# LOCATION 's3://data-lake-temp-tables/SNOP/final_data_new/'