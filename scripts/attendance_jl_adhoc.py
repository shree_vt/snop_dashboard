import boto3, csv, time, sys, os, psycopg2,pandas as pd, mysql.connector, re, numpy as np
s3 = boto3.resource('s3')
from datetime import datetime
from datetime import date
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from google.auth.transport.requests import Request
import pandas as pd
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
SPREADSHEET_ID = "1ntBSfvrZ_54BoZLYx8iPv6UxJK3vVItv-CUuzLwwIis"

def pull_sheet_data(SCOPES,Id,Rn):
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token3.pickle'):
        with open('token3.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                r"C:\Users\delhivery\Documents\scripts\oauth_snop2.json", SCOPES)
            
            creds = flow.run_local_server(port=8080)
        # Save the credentials for the next run
        with open('token3.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=Id,
                                range=Rn).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        rows = sheet.values().get(spreadsheetId=Id,
                                  range=Rn).execute()
        data = rows.get('values')
        print("COMPLETE: Data copied")
        df = pd.DataFrame(data)
        df.columns = df.iloc[0]
        df = df.drop(df.index[0])
        return df

df_jl = pull_sheet_data(SCOPES,SPREADSHEET_ID , "jl_final!B1:Y")
df_adhoc = pull_sheet_data(SCOPES,SPREADSHEET_ID , "adhoc_final!A1:C")

