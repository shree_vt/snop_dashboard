#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import boto3, csv, time, sys, os, psycopg2,pandas as pd, mysql.connector, re, numpy as np


# In[ ]:


s3 = boto3.resource('s3')


# In[ ]:


from datetime import datetime


# In[ ]:


from datetime import date


# In[ ]:


def run_query(client, query, database, s3_output):
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )
    # print('Execution ID: ' + response['QueryExecutionId'])
    return response


# In[ ]:


def get_query_status(client, Execution_Id):
    response = client.get_query_execution(
        QueryExecutionId=Execution_Id
    )
    query_id = response['QueryExecution']['QueryExecutionId']
    status = response['QueryExecution']['Status']['State']
    print('Started')
    print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    while status not in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
        # print('|' + status, end='')
        time.sleep(30)
        response = client.get_query_execution(
            QueryExecutionId=Execution_Id
        )
        status = response['QueryExecution']['Status']['State']
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    if status in ['SUCCEEDED']:
        return True
    if status in ['FAILED', 'CANCELLED']:
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))
        print("Failure Reason: {}".format(
            response['QueryExecution']['Status'].get('StateChangeReason', 'Error Unknown')))
        return False
    else:
        return False


# In[ ]:


def run_query_to_athena(query, output_filename):
    client = boto3.client('athena', 'us-east-1' )
    s3_output = "s3://aws-athena-query-results-190020191201-us-east-1/"
    s3_bucket="aws-athena-query-results-190020191201-us-east-1"
    database = 'express_dwh'
    res = run_query(client, query, database, s3_output)

    q_id = res['QueryExecutionId']
    if get_query_status(client, q_id):
        print("Query Successful. Downloading data: ")
        os.system("aws s3 cp " + s3_output + q_id + ".csv  {}".format(output_filename))
        s3_key = q_id + '.csv'
        local_filename = output_filename + '.csv'
        s3.Bucket(s3_bucket).download_file(s3_key, local_filename)

        return True
    else:
        print("Query Unsuccessful. Please check the query or the Athena server status")
        return False


# In[ ]:


query_act = """
WITH facility_data as
(
        select facility_code,facility_name,facility_city,facility_type,facility_zone,property_country,property_id
        from
        (
        select "property_facility_facility_code" as facility_code,"property_facility_name" as facility_name,"property_city" as facility_city,
                property_facility_facility_type facility_type,property_zone facility_zone,property_country,property_id
                ,row_number() over (partition by "property_facility_facility_code" order by action_date desc) as row
        from facility_ad_json
        ) as t
        where row = 1
        and property_country = 'India'
)

,ivd_scan_data as
(
    SELECT cs_sl sl,date(ivd + interval '330' minute) as sd_date,pdt as bg_typ,pid2 as bag
    FROM
    (   
      SELECT cs_sl,ivd,wbn,pdt,case when pid is null then wbn else pid end as pid2,
            row_number() over (partition BY wbn ORDER BY action_date DESC) AS ROW
      FROM express_dwh_6m.package_scan_latest_pq_6m
      where ad >= date_format((date_trunc('day',timestamp '2020-07-01') - interval '6' day) - interval '00' minute, '%Y-%m-%d-%H')
      and ad <= date_format((date_trunc('day',timestamp '2020-07-10') + interval '1' day) - interval '00' minute, '%Y-%m-%d-%H')
      and "ivd" >= (date_trunc('day',timestamp '2020-07-01') - interval '5' day) - interval '330' minute
      and "ivd" <= (date_trunc('day',timestamp '2020-07-10') + interval '1' day) - interval '330' minute
      and ocid = "cs_slid"
      and "cs_slid" in (select facility_code from facility_data where lower(facility_type) like '%hub%')
    --   AND large = TRUE
      AND pdt in ('B2B','Heavy') 
    )
    WHERE ROW = 1
    and date(ivd + interval '330' minute) >= timestamp '2020-07-01' and date(ivd + interval '330' minute) <= timestamp '2020-07-10'
)

, bag_data_created_oc_sl as
(
    select bg,bag_sl,oc,act,bagmt,ocid
    from
    (
        select "cs_sl" bag_sl,"oc" oc,"cs_act" act,t as bagmt,"bs" bg,ocid ocid
                ,row_number() over(partition by bs order by action_date desc) as row
        from bag_s3_parquet
        where ad >= date_format((date_trunc('day',timestamp '2020-07-01') - interval '6' day) - interval '00' minute, '%Y-%m-%d-%H')
        and ad <= date_format((date_trunc('day',timestamp '2020-07-10') + interval '1' day) - interval '00' minute, '%Y-%m-%d-%H')
        and cd >= (date_trunc('day',timestamp '2020-07-01') - interval '5' day) - interval '330' minute
        and cd <= (date_trunc('day',timestamp '2020-07-10') + interval '1' day) - interval '330' minute
        and ocid = "cs_slid"
        and "cs_slid" in (select facility_code from facility_data where lower(facility_type) like '%hub%')
    )
    where row = 1
)
, bag_data_created_for_cn_sl as
(
    select bg,bag_sl,oc,act,bagmt,cnid
    from
    (
        select "cs_sl" bag_sl,"oc" oc,"cs_act" act,"t" bagmt,"bs" bg,cnid cnid
                ,row_number() over(partition by bs order by action_date desc) as row
        from bag_s3_parquet
        where ad >= date_format((date_trunc('day',timestamp '2020-07-01') - interval '6' day) - interval '00' minute, '%Y-%m-%d-%H')
        and ad <= date_format((date_trunc('day',timestamp '2020-07-10') + interval '1' day) - interval '00' minute, '%Y-%m-%d-%H')
        and cd >= (date_trunc('day',timestamp '2020-07-01') - interval '5' day) - interval '330' minute
        and cd <= (date_trunc('day',timestamp '2020-07-10') + interval '1' day) - interval '330' minute
        and cnid = "cs_slid"
        and "cs_slid" in (select facility_code from facility_data where lower(facility_type) like '%hub%')
    )
    where row = 1
)
, wbn_bag_data as
(
    select sl,DATE(sd) sd_date,act,slid
            ,case when act in ('<C','+C') then 'Colocated'
                  when act in ('<L','+L') then 'FTL'
                end oc_typ
            ,case when act in ('<C','<L') then 'In Bound'
                  when act in ('+C','+L') then 'Out Bound'
                end flow
            ,case when pdt = 'B2B' then 'B2B' when pdt = 'Heavy' then 'Heavy' else 'B2C' end bg_typ
            ,bag
    from
        (
            select *,pid2 bag,(cs_sd2 + interval '330' minute) as sd
                    ,row_number() over(partition by pid2,sl,act order by action_date ) as row
            from
            (select "wbn","pdt","cs_sd",sl,nsl,slid,action_date,cnid,
              case when nsl = 'CS-CSL' then '<C'
              when sl_id = cn_id then '+C'
              when lead(nsl) over(partition by wbn order by action_date) = 'CS-CSL' then '+C'
              else cs_act end as act,
              case when nsl = 'CS-CSL' then case when pid is null then wbn else pid end
              when sl_id = cn_id then case when lead(pid) over(partition by wbn order by action_date) is null then wbn else lead(pid) over(partition by wbn order by action_date) end
              when lead(nsl) over(partition by wbn order by action_date) = 'CS-CSL'
                            then case when lead(pid) over(partition by wbn order by action_date) is null then wbn else lead(pid) over(partition by wbn order by action_date) end
              else pid end as pid2,
              case when nsl = 'CS-CSL' then cs_sd
              when sl_id = cn_id then lead("cs_sd") over(partition by wbn order by action_date)
              when lead(nsl) over(partition by wbn order by action_date) = 'CS-CSL' then lead("cs_sd") over(partition by wbn order by action_date)
              else cs_sd end as cs_sd2
              from
              (select wbn,pdt,cs_sd,sl,nsl,slid,action_date,cnid,cs_act,pid,f1.property_id as sl_id,f2.property_id as cn_id from
                (select "wbn","pdt","cs_sd","cs_sl" sl,"cs_nsl" nsl,"cs_slid" slid,action_date,cnid
                ,cs_act
                ,pid
                from express_dwh_6m.package_scan_latest_pq_6m
                where ad >= date_format((date_trunc('day',timestamp '2020-07-01') - interval '6' day) - interval '00' minute, '%Y-%m-%d-%H')
                and ad <= date_format((date_trunc('day',timestamp '2020-07-10') + interval '1' day) - interval '00' minute, '%Y-%m-%d-%H')
                and "cs_sd" >= (date_trunc('day',timestamp '2020-07-01') - interval '5' day) - interval '330' minute
                and "cs_sd" <= (date_trunc('day',timestamp '2020-07-10') + interval '1' day) - interval '330' minute
                and "cs_slid" in (select facility_code from facility_data where lower(facility_type) like '%hub%')
                -- and (cs_act in ('<C','+C','+L') or cs_nsl='CS-CSL' or (cs_act='<L' and cs_nsl<>'X-ILL2F'))
                -- and action_type = 'scan'
                ) a left join (select facility_code, property_id from facility_data) f1 on a.slid = f1.facility_code left join (select facility_code, property_id from facility_data) f2 on a.cnid = f2.facility_code)
            )
            where (act in ('<C','+C','+L') or nsl='CS-CSL' or (act='<L' and nsl<>'X-ILL2F'))
        )
    where row = 1
    and slid in (select facility_code from facility_data where lower(facility_type) like '%hub%')
    and date(sd) >= timestamp '2020-07-01' and date(sd) <= timestamp '2020-07-10'
    -- group by sl,date(sd),sd_h,oc_typ,flow,bg_typ
)


select sl,sd_date,oc_typ,flow
        ,bg_typ
        ,count(distinct bag) Bags
from
    (
        select sl,sd_date,oc_typ,flow,bg_typ,bag
        from wbn_bag_data

        union all

        select sl,sd_date,'Colocated' oc_typ,'In Bound' as flow,bg_typ,bag
        from wbn_bag_data w1
        join bag_data_created_oc_sl on bg = bag and w1.act = '+L' and slid = ocid and bg not like 'BAGL%' 

        union all

        select sl,sd_date,'Colocated' oc_typ,'Out Bound' as flow,bg_typ,bag
        from wbn_bag_data w1
        join bag_data_created_for_cn_sl on bg = bag and w1.act = '<L' and slid = cnid

        union all

        select sl,sd_date,'Colocated' oc_typ,'In Bound' as flow,bg_typ,bag
        from ivd_scan_data
    )
group by 1,2,3,4,5


"""


# In[ ]:


dump_path = "bags"
result=run_query_to_athena(query_act, dump_path)


# In[ ]:


local_filename= 'bags' + '.csv'
output_act=pd.read_csv(local_filename)


# In[ ]:


output_act.head()


# In[ ]:


from sqlalchemy import create_engine


# In[ ]:


import sqlalchemy


# In[ ]:


engine = create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                              format("shree", "snop321#",
                                                     "datawarehouse-lastmile-analytics-rds.ceypiyhweprx.us-east-1.rds.amazonaws.com", "SNOP"))


# In[ ]:


cnx = engine.raw_connection()


# In[ ]:


output_act.to_sql(name='hub_bags', con=engine, chunksize=1000, if_exists = 'append', index=False,  dtype={'sd_date': sqlalchemy.DateTime()})


# In[ ]:


cnx.close()

