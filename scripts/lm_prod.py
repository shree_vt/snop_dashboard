#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import boto3, csv, time, sys, os, psycopg2,pandas as pd, mysql.connector, re, numpy as np


# In[ ]:


s3 = boto3.resource('s3')


# In[ ]:


from datetime import datetime


# In[ ]:


from datetime import date


# In[ ]:


def run_query(client, query, database, s3_output):
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )
    # print('Execution ID: ' + response['QueryExecutionId'])
    return response


# In[ ]:


def get_query_status(client, Execution_Id):
    response = client.get_query_execution(
        QueryExecutionId=Execution_Id
    )
    query_id = response['QueryExecution']['QueryExecutionId']
    status = response['QueryExecution']['Status']['State']
    print('Started')
    print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    while status not in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
        # print('|' + status, end='')
        time.sleep(30)
        response = client.get_query_execution(
            QueryExecutionId=Execution_Id
        )
        status = response['QueryExecution']['Status']['State']
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    if status in ['SUCCEEDED']:
        return True
    if status in ['FAILED', 'CANCELLED']:
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))
        print("Failure Reason: {}".format(
            response['QueryExecution']['Status'].get('StateChangeReason', 'Error Unknown')))
        return False
    else:
        return False


# In[ ]:


def run_query_to_athena(query, output_filename):
    client = boto3.client('athena', 'us-east-1' )
    s3_output = "s3://aws-athena-query-results-190020191201-us-east-1/"
    s3_bucket="aws-athena-query-results-190020191201-us-east-1"
    database = 'express_dwh'
    res = run_query(client, query, database, s3_output)

    q_id = res['QueryExecutionId']
    if get_query_status(client, q_id):
        print("Query Successful. Downloading data: ")
        os.system("aws s3 cp " + s3_output + q_id + ".csv  {}".format(output_filename))
        s3_key = q_id + '.csv'
        local_filename = output_filename + '.csv'
        s3.Bucket(s3_bucket).download_file(s3_key, local_filename)

        return True
    else:
        print("Query Unsuccessful. Please check the query or the Athena server status")
        return False


# In[ ]:


query_act = """
with
cn_list
as
(
select id, cn_type,city, state,cn||' ('||state||')' as hq, region
from
(
select "property_facility_facility_code" as id ,  property_zone as region,  "property_active" , property_country , "property_facility_name" as cn,"property_facility_facility_type" as cn_type, property_city as city,  property_state as state, row_number() over (partition by "property_facility_facility_code"  order by "action_date" desc) as row
from express_dwh.facility_ad_json
)
where row=1
and upper(cn_type) in ('DC', 'DC,PC')
and not upper(cn) like '%DPP%' 
and not upper(cn) like '%3PL%' 
and  upper(property_country) = 'INDIA'
and not upper(cn) like 'TEST%'
-- and cn ='' --ENTER THE FACILITY NAME
)

select cn,city,region,state,dpd_month,dpd_date,mode,vt,mandays,total_dispatches,total_closures from (

select date_format(dpd+interval '330' minute,'%M') as dpd_month,date(dpd+interval '330' minute) as dpd_date,cn,mode,vt,sum(wbn_count) total_dispatches,sum(closure) total_closures,count(distinct concat(cast(date(dpd + interval '330' minute) as varchar),fu_emp_id)) mandays from

 (
SELECT dpd,cn,fu_emp_id,
case when length(fu_emp_id) = 6 then 'regular'
    when lower(md) in ('agent','gig','adhoc','self_collect') then lower(md)
     else 'others' end as "mode",vt
,wbn_count,coalesce(eod_pu,0) + coalesce(eod_dv,0) closure
        FROM
            (
                SELECT dpd,cn,cnid,dwbn,ds,dn,wbn_count,md,closure_percentage,eod_amt,eod_skm,eod_ekm,eod_exp_cod,fu_emp_id,vn,vt,fu_name,eod_pu,eod_dv,si_earned
                    ,row_number() over (partition BY dwbn ORDER BY action_date DESC) AS ROW
                FROM express_dwh.dispatch_lm_s3_parquet
                WHERE ad >= date_format((date_trunc('day',CURRENT_TIMESTAMP) - interval '4' day) - interval '00' minute, '%Y-%m-%d-%H')
                AND ad <= date_format((date_trunc('day',CURRENT_TIMESTAMP) - interval '0' DAY) - interval '00' MINUTE, '%Y-%m-%d-%H')
                AND dpd >= (date_trunc('day',CURRENT_TIMESTAMP) - interval '1' day) - interval '330' minute
                AND dpd <= (date_trunc('day',CURRENT_TIMESTAMP) - interval '0' DAY) - interval '330' MINUTE
                and cnid in (select distinct id from cn_list)
            )
        WHERE ROW = 1)  

group by 1,2,3,4,5
) a

left join cn_list b
on 
a.cn=b.hq
"""


# In[ ]:


dump_path = "lm_prod_act"
result=run_query_to_athena(query_act, dump_path)


# In[ ]:


local_filename= 'lm_prod_act' + '.csv'
output_act=pd.read_csv(local_filename)


# In[ ]:


output_act.head()


# In[ ]:


from sqlalchemy import create_engine


# In[ ]:


import sqlalchemy


# In[ ]:


engine = create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                              format("shree", "snop321#",
                                                     "datawarehouse-lastmile-analytics-rds.ceypiyhweprx.us-east-1.rds.amazonaws.com", "SNOP"))


# In[ ]:


cnx = engine.raw_connection()


# In[ ]:


output_act.to_sql(name='lm_productivity', con=engine, chunksize=1000, if_exists = 'append', index=False,  dtype={'dpd_date': sqlalchemy.DateTime()})


# In[ ]:


cnx.close()

