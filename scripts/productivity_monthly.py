#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import boto3, csv, time, sys, os, psycopg2,pandas as pd, mysql.connector, re, numpy as np


# In[ ]:


s3 = boto3.resource('s3')


# In[ ]:


from datetime import datetime


# In[ ]:


from datetime import date


# In[ ]:


def run_query(client, query, database, s3_output):
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )
    # print('Execution ID: ' + response['QueryExecutionId'])
    return response


# In[ ]:


def get_query_status(client, Execution_Id):
    response = client.get_query_execution(
        QueryExecutionId=Execution_Id
    )
    query_id = response['QueryExecution']['QueryExecutionId']
    status = response['QueryExecution']['Status']['State']
    print('Started')
    print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    while status not in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
        # print('|' + status, end='')
        time.sleep(30)
        response = client.get_query_execution(
            QueryExecutionId=Execution_Id
        )
        status = response['QueryExecution']['Status']['State']
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    if status in ['SUCCEEDED']:
        return True
    if status in ['FAILED', 'CANCELLED']:
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))
        print("Failure Reason: {}".format(
            response['QueryExecution']['Status'].get('StateChangeReason', 'Error Unknown')))
        return False
    else:
        return False


# In[ ]:


def run_query_to_athena(query, output_filename):
    client = boto3.client('athena', 'us-east-1' )
    s3_output = "s3://aws-athena-query-results-190020191201-us-east-1/"
    s3_bucket="aws-athena-query-results-190020191201-us-east-1"
    database = 'express_dwh'
    res = run_query(client, query, database, s3_output)

    q_id = res['QueryExecutionId']
    if get_query_status(client, q_id):
        print("Query Successful. Downloading data: ")
        os.system("aws s3 cp " + s3_output + q_id + ".csv  {}".format(output_filename))
        s3_key = q_id + '.csv'
        local_filename = output_filename + '.csv'
        s3.Bucket(s3_bucket).download_file(s3_key, local_filename)

        return True
    else:
        print("Query Unsuccessful. Please check the query or the Athena server status")
        return False


# In[ ]:


query = """
select cn,facility_city as city,facility_zone as region,facility_state as state,dpd_month,mode,vt,mandays,total_dispatches,total_closures from (
	select facility_code,facility_name,facility_city,replace(facility_type,',','/') facility_type,facility_zone,facility_state from(
		select "property_facility_facility_code" as facility_code,"property_facility_name" as facility_name,"property_city" as facility_city, 
		property_facility_facility_type facility_type,property_zone facility_zone,property_facility_active,property_country,property_state facility_state ,
		row_number() over (partition by "property_facility_facility_code" order by action_date desc) as row from facility_ad_json) as t 
	where row = 1 and property_country = 'India' and facility_type like '%DC%')
	join (
		Select date_format(dpd+interval '330' MINUTE,'%M') dpd_month,cn,cnid,mode,vt,count(fu_emp_id) as mandays,sum(wbn_count) as total_dispatches, sum(closed) as total_closures 
		from (
			select dpd, cn,cnid, dwbn, fu_emp_id, wbn_count, eod_dv, eod_pu,vt,coalesce(eod_dv,0) + coalesce(eod_pu,0) as closed,
			case when md = 'regular' and length(fu_emp_id) = 6 then 'regular'
			when md = 'agent' then 'agent'
			when md = 'gig' then 'gig'
			when md = 'adhoc' then 'adhoc'
			when md = 'self_collect' then 'self_collect'
			else 'others' end as mode,

			row_number() over (partition by dwbn order  by action_date desc) rnum 
			from dispatch_lm_s3_parquet 
			where ad >= date_format((date_trunc('day',timestamp '2019-11-01') - interval '3' day) - interval '00' minute, '%Y-%m-%d-%H') 
			and ad <= date_format((date_trunc('day',timestamp '2020-04-12') + interval '2' day) - interval '00' minute, '%Y-%m-%d-%H') 
			and dpd >= ((date_trunc('day',timestamp '2019-11-01') - interval '0' day) - interval '330' minute) 
			and dpd <=  ((date_trunc('day',timestamp '2020-04-12') +interval '1' day) - interval '330' minute ) ) 
		where rnum=1 
		group by 1,2,3,4,5) 
	on cnid = facility_code
"""


# In[ ]:


dump_path = "prod_test"
result=run_query_to_athena(query, dump_path)


# In[ ]:


local_filename= 'prod_test' + '.csv'
output=pd.read_csv(local_filename)


# In[ ]:


output.head()


# In[ ]:


from sqlalchemy import create_engine


# In[ ]:


import sqlalchemy


# In[ ]:


engine = create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                              format("shree", "snop321#",
                                                     "datawarehouse-lastmile-analytics-rds.ceypiyhweprx.us-east-1.rds.amazonaws.com", "SNOP"))


# In[ ]:


cnx = engine.raw_connection()


# In[ ]:


output.to_sql(name='lm_dc_monthly_productivity', con=engine, chunksize=1000, if_exists = 'append', index=False)


# In[ ]:


cnx.close()

