import boto3, csv, time, sys, os, psycopg2,pandas as pd, mysql.connector, re, numpy as np


# In[ ]:


s3 = boto3.resource('s3')


# In[ ]:


from datetime import datetime


# In[ ]:


from datetime import date


# In[ ]:


def run_query(client, query, database, s3_output):
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )
    # print('Execution ID: ' + response['QueryExecutionId'])
    return response


# In[ ]:


def get_query_status(client, Execution_Id):
    response = client.get_query_execution(
        QueryExecutionId=Execution_Id
    )
    query_id = response['QueryExecution']['QueryExecutionId']
    status = response['QueryExecution']['Status']['State']
    print('Started')
    print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    while status not in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
        # print('|' + status, end='')
        time.sleep(30)
        response = client.get_query_execution(
            QueryExecutionId=Execution_Id
        )
        status = response['QueryExecution']['Status']['State']
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    if status in ['SUCCEEDED']:
        return True
    if status in ['FAILED', 'CANCELLED']:
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))
        print("Failure Reason: {}".format(
            response['QueryExecution']['Status'].get('StateChangeReason', 'Error Unknown')))
        return False
    else:
        return False


# In[ ]:


def run_query_to_athena(query, output_filename):
    client = boto3.client('athena', 'us-east-1' )
    s3_output = "s3://aws-athena-query-results-190020191201-us-east-1/"
    s3_bucket="aws-athena-query-results-190020191201-us-east-1"
    database = 'express_dwh'
    res = run_query(client, query, database, s3_output)

    q_id = res['QueryExecutionId']
    if get_query_status(client, q_id):
        print("Query Successful. Downloading data: ")
        os.system("aws s3 cp " + s3_output + q_id + ".csv  {}".format(output_filename))
        s3_key = q_id + '.csv'
        local_filename = output_filename + '.csv'
        s3.Bucket(s3_bucket).download_file(s3_key, local_filename)

        return True
    else:
        print("Query Unsuccessful. Please check the query or the Athena server status")
        return False


# In[ ]:


query_act = """
select cn,facility_city as city,facility_zone as region,facility_state as state,city_type as tier,cpd_date,Client,Cat,act
from
(
select cn, cnid, cpd_date, Client, 'Actual' cat,sum(box) act from
(select date("date_cpd"+interval '330' minute) cpd_date,cn,cnid,
cl as "Client"
,"pt",
    (case when ( pdt='' or pdt is null)  then 'B2C' else pdt end) product_typ,
    (case when pdt like '%Heavy%' then 'Heavy' when cl like '%B2B%' then 'B2B' else 'B2C' end) prtyp,zn,
    count(distinct master) vol,
    count(wbn) box
from
(select "date_cpd",date_mnd,"cl","pt",mcount,zn
,(case when date_mnd is null and cl like '%B2B%' then 'B2B' when cl='Flipkart Heavy' then 'Heavy' else "pdt" end) pdt
,cn,cnid,"wbn",master
from 
(select "date_cpd",date_mnd,"cl","pt",mcount,zn,"pdt",cn,cnid,"wbn","mwn",(case when "mwn" is null then wbn else mwn end) master
,row_number() over (partition by "wbn" order by cs_sd desc,action_date desc) as row
from "express_dwh_3m"."package_scan_latest_pq_3m"
where ad > date_format((date_trunc('day',current_timestamp) - interval '5' day) - interval '00' minute, '%Y-%m-%d-%H')
--and "_pt" in ('COD','Pre-paid')
and NOT ("cl" in ('Delhivery','Skynet Logistics Outbound','Skynet Logistics Inbound','FOOD4THOUGHTFOUNDATION B2B','WOLWYNB2B','DEL LS','Quikr Pilot','Delhivery E POD','AIPEXWORLDWIDE B2B','C2C Pilot','WalletTest3','4PETNEEDS B2B','Infra B2B','6YCOLLECTIVE B2B','DLV Internal Test','PALLETTRACKING B2B','C2CQuikr','DELHIVERYCHEQUES B2B','DARAZBNTEST EXPRESS','AJKERDEALTEST EXPRESS','XBBNTEST EXPRESS','DARAZNPTEST EXPRESS','DELHIVERY INTERCHANGE B2B'))
and date_cpd between  (date_trunc('day',current_timestamp + interval '330' minute) - interval '330' minute - interval '1' Day) and (date_trunc('day',current_timestamp + interval '0' day + interval '330' minute) - interval '330' minute)
)
where row=1 
)
group by 1,2,3,4,5,6,7,8)
where prtyp = 'B2C'
group by 1,2,3,4,5
        )
left join
(select facility_code,facility_name,facility_city,replace(facility_type,',','/') facility_type,facility_zone,facility_state,facility_name||' ('||facility_state||')' as hq
        from
        (
        select "property_facility_facility_code" as facility_code,"property_facility_name" as facility_name,"property_city" as facility_city,
                property_facility_facility_type facility_type,property_zone facility_zone,property_facility_active,property_country,property_state facility_state
                ,row_number() over (partition by "property_facility_facility_code" order by action_date desc) as row
        from facility_ad_json
        ) as t
        where row = 1

  )
  on cn = hq
  left join
  (select center_hq,city_type from express_dwh.tmp_ct_cn_sm_mapping
  ) on cn = center_hq
"""


# In[ ]:


dump_path = "lm_act"
result=run_query_to_athena(query_act, dump_path)


# In[ ]:


local_filename= 'lm_act' + '.csv'
output_act=pd.read_csv(local_filename)


# In[ ]:


output_act.head()


# In[ ]:


from sqlalchemy import create_engine


# In[ ]:


import sqlalchemy


# In[ ]:


engine = create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                              format("shree", "snop321#",
                                                     "datawarehouse-lastmile-analytics-rds.ceypiyhweprx.us-east-1.rds.amazonaws.com", "SNOP"))


# In[ ]:


cnx = engine.raw_connection()


# In[ ]:


output_act.to_sql(name='cn_cl_lm_actual', con=engine, chunksize=1000, if_exists = 'append', index=False,  dtype={'cpd_date': sqlalchemy.DateTime()})


# In[ ]:


cnx.close()
