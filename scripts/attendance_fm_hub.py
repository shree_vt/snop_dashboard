#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import boto3, csv, time, sys, os, psycopg2,pandas as pd, mysql.connector, re, numpy as np


# In[ ]:


s3 = boto3.resource('s3')


# In[ ]:


from datetime import datetime


# In[ ]:


from datetime import date


# In[ ]:


def run_query(client, query, database, s3_output):
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )
    # print('Execution ID: ' + response['QueryExecutionId'])
    return response


# In[ ]:


def get_query_status(client, Execution_Id):
    response = client.get_query_execution(
        QueryExecutionId=Execution_Id
    )
    query_id = response['QueryExecution']['QueryExecutionId']
    status = response['QueryExecution']['Status']['State']
    print('Started')
    print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    while status not in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
        # print('|' + status, end='')
        time.sleep(30)
        response = client.get_query_execution(
            QueryExecutionId=Execution_Id
        )
        status = response['QueryExecution']['Status']['State']
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    if status in ['SUCCEEDED']:
        return True
    if status in ['FAILED', 'CANCELLED']:
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))
        print("Failure Reason: {}".format(
            response['QueryExecution']['Status'].get('StateChangeReason', 'Error Unknown')))
        return False
    else:
        return False


# In[ ]:


def run_query_to_athena(query, output_filename):
    client = boto3.client('athena', 'us-east-1' )
    s3_output = "s3://aws-athena-query-results-190020191201-us-east-1/"
    s3_bucket="aws-athena-query-results-190020191201-us-east-1"
    database = 'express_dwh'
    res = run_query(client, query, database, s3_output)

    q_id = res['QueryExecutionId']
    if get_query_status(client, q_id):
        print("Query Successful. Downloading data: ")
        os.system("aws s3 cp " + s3_output + q_id + ".csv  {}".format(output_filename))
        s3_key = q_id + '.csv'
        local_filename = output_filename + '.csv'
        s3.Bucket(s3_bucket).download_file(s3_key, local_filename)

        return True
    else:
        print("Query Unsuccessful. Please check the query or the Athena server status")
        return False


# In[ ]:



query_act = """
with facility_data as (select facility_code,facility_name,facility_city,facility_type,facility_zone
        from
        (
        select "property_facility_facility_code" as facility_code,"property_facility_name" as facility_name,"property_city" as facility_city,
                property_facility_facility_type facility_type,property_zone facility_zone,property_facility_active,property_country
                ,row_number() over (partition by "property_facility_facility_code" order by action_date desc) as row
        from 
        express_dwh.facility_ad_json
        ) as t
        where row = 1
        and (facility_type like '%HUB%' or facility_type like '%PC%' or facility_type like '%CP%')
        and property_country = 'India')
        
 select shift_date,eid,d.facility_name,att_status,Business_Unit,designation,level_1,level_2,Centre_Type,user_type,offroll_tag,onroll_tag from 

    (
        (select shift_date,eid,att_status,"Business_Unit", designation,level_1,level_2,"Centre_Type",user_type,facility_code,
case when (user_type = 'OFR' and designation in ('Ground Coordinator','Field Executive') and level_1 in ('First Mile','Line Haul')) then 
    case when level_2 = 'FM_B2B' then 'fm_b2b' 
        when level_2 in ('Airport Staff','HUB','HUB Mix Bag Processing') then 'hub'
        when level_2 in ('PC_Pickup','CP_Pickup') then 'pickup'
        when level_2 in ('PC_Processing','CP_Processing') then 'processing'
    else '-' end 
else '-' end as "offroll_tag" ,
case when (user_type = 'OR' and designation in ('Executive','Senior Executive','Executive (D0140)')) then 
    case when level_2 in ('FM_B2B','FM B2B') then 'fm_b2b' 
        when level_2 in ('Hub & Line Haul Operations') then 'hub'
        when level_2 in ('Pick Up Coordination','Pickup Operations','Pick-up & Processing Operations','CP Operations') then 'pickup'
        when level_2 in ('Returns Operations','Processing Operations','Sorter Operation') then 'processing'
    else '-' end 
else '-' end as "onroll_tag" 
from(
    
    select shift_date,eid,att_status,businessunit as "Business_Unit", designation,darwin_facility_id as facility_code,subdepartment as level_1,department as level_2,officecentertype as "Centre_Type",user_type 
    from (
        select * from (
            select att_status,shift_date,eid,row_number() over (partition BY eid,shift_date ORDER BY action_date DESC) AS ROW 
            from attendance_ad_json 
            where shift_date in ('01-08-2020',  '02-08-2020',   '03-08-2020',   '04-08-2020',   '05-08-2020',   '06-08-2020',   '07-08-2020')
            and att_status = 1
            ) 
        where ROW =1
        ) a 
    
    inner join
    
    (
        select * from (
            select employee_id,businessunit,designation,darwin_facility_id,department,officecentertype,subdepartment,user_type,row_number() over (partition BY employee_id ORDER BY action_date DESC) AS ROW 
            from express_dwh.ums_user_ad_json 
            where darwin_facility_id in (select facility_code from facility_data)
            )
        where ROW = 1
        ) b 
    
    on a.eid = b.employee_id
    )
    ) c 
    
    left join facility_data as d 
    
    on c.facility_code = d.facility_code)


"""


# In[ ]:


dump_path = "attendance_fm_hub"
result=run_query_to_athena(query_act, dump_path)


# In[ ]:


local_filename= 'attendance_fm_hub' + '.csv'
output_act=pd.read_csv(local_filename)


# In[ ]:


output_act.head()


# In[ ]:


from sqlalchemy import create_engine


# In[ ]:


import sqlalchemy


# In[ ]:


engine = create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                              format("shree", "snop321#",
                                                     "datawarehouse-lastmile-analytics-rds.ceypiyhweprx.us-east-1.rds.amazonaws.com", "SNOP"))


# In[ ]:


cnx = engine.raw_connection()


# In[ ]:


output_act.to_sql(name='attendance_fm_hub', con=engine, chunksize=1000, if_exists = 'append', index=False)


# In[ ]:


cnx.close()

