#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import boto3, csv, time, sys, os, psycopg2,pandas as pd, mysql.connector, re, numpy as np


# In[ ]:


s3 = boto3.resource('s3')


# In[ ]:


from datetime import datetime


# In[ ]:


from datetime import date


# In[ ]:


def run_query(client, query, database, s3_output):
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )
    # print('Execution ID: ' + response['QueryExecutionId'])
    return response


# In[ ]:


def get_query_status(client, Execution_Id):
    response = client.get_query_execution(
        QueryExecutionId=Execution_Id
    )
    query_id = response['QueryExecution']['QueryExecutionId']
    status = response['QueryExecution']['Status']['State']
    print('Started')
    print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    while status not in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
        # print('|' + status, end='')
        time.sleep(30)
        response = client.get_query_execution(
            QueryExecutionId=Execution_Id
        )
        status = response['QueryExecution']['Status']['State']
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    if status in ['SUCCEEDED']:
        return True
    if status in ['FAILED', 'CANCELLED']:
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))
        print("Failure Reason: {}".format(
            response['QueryExecution']['Status'].get('StateChangeReason', 'Error Unknown')))
        return False
    else:
        return False


# In[ ]:


def run_query_to_athena(query, output_filename):
    client = boto3.client('athena', 'us-east-1' )
    s3_output = "s3://aws-athena-query-results-190020191201-us-east-1/"
    s3_bucket="aws-athena-query-results-190020191201-us-east-1"
    database = 'express_dwh'
    res = run_query(client, query, database, s3_output)

    q_id = res['QueryExecutionId']
    if get_query_status(client, q_id):
        print("Query Successful. Downloading data: ")
        os.system("aws s3 cp " + s3_output + q_id + ".csv  {}".format(output_filename))
        s3_key = q_id + '.csv'
        local_filename = output_filename + '.csv'
        s3.Bucket(s3_bucket).download_file(s3_key, local_filename)

        return True
    else:
        print("Query Unsuccessful. Please check the query or the Athena server status")
        return False


# In[ ]:


query_act = """
select cn,facility_city as city,facility_zone as region,facility_state as state,cpd_date,Client,Cat,act
from
(select facility_code,facility_name,facility_city,replace(facility_type,',','/') facility_type,facility_zone,facility_state
        from
        (
        select "property_facility_facility_code" as facility_code,"property_facility_name" as facility_name,"property_city" as facility_city,
                property_facility_facility_type facility_type,property_zone facility_zone,property_facility_active,property_country,property_state facility_state
                ,row_number() over (partition by "property_facility_facility_code" order by action_date desc) as row
        from facility_ad_json
        ) as t
        where row = 1
        and property_country = 'India'
        -- and facility_type like '%DC%'
        )

join
(
SELECT * FROM(
SELECT cn,cnid,cpd_date,
          CASE
          WHEN cl IN ('AMAZONCRETURNS','AMAZONINDIA') THEN 'Amazon'
          WHEN cl = 'AEPL' THEN 'Glowroad'
          WHEN cl IN ('Flipkart','FLIPKART - E2E FOOD','FLIPKART E2E','FLIPKART SURFACE') THEN 'Flipkart'
          WHEN cl IN ('FTPL','FTPL SB','FTPL SURFACE') THEN 'Meesho'
          WHEN cl IN ('LimeRoad','LIMEROAD SURFACE') THEN 'Limeroad'
          WHEN cl IN ('CLUBFACTORY SURFACE','CLUBFACTORYCN SURFACE','CLUBFACTORYIN SURFACE') THEN 'Clubfactory'
          WHEN cl IN ('Snapdeal RL','SNAPDEAL SURFACE','SNAPDEAL DPP') THEN 'Snapdeal'
          WHEN cl IN ('UDAAN','UDAAN SURFACE') THEN 'Udaan'
          else cl end AS "Client",
          'Actual' AS "Cat",
          sum(wbn_count) as act
  FROM
  (
    SELECT cn,cnid,cl,pdt,date(date_cpd + interval '330' MINUTE) AS cpd_date,count(wbn) AS wbn_count
    FROM
    (
      SELECT date_cpd,cl,cn,cnid,wbn,pdt,
 row_number() over (partition BY wbn ORDER BY action_date DESC) AS ROW
      FROM express_dwh_3m.package_scan_latest_pq_3m
      WHERE ad >= date_format((date_trunc('day',CURRENT_DATE) - interval '10' DAY) - interval '00' MINUTE, '%Y-%m-%d-%H')
      AND ad <= date_format((date_trunc('day',CURRENT_DATE) - interval '0' DAY) - interval '00' MINUTE, '%Y-%m-%d-%H')
      AND cs_sd >= (date_trunc('day',CURRENT_TIMESTAMP) - interval '5' DAY) - interval '330' MINUTE
      AND cs_sd <= (date_trunc('day',CURRENT_TIMESTAMP) - interval '0' DAY) - interval '330' MINUTE 
      AND cl NOT IN ('Delhivery','Skynet Logistics Outbound','Skynet Logistics Inbound','FOOD4THOUGHTFOUNDATION B2B','WOLWYNB2B','DEL LS','Quikr Pilot','Delhivery E POD','AIPEXWORLDWIDE B2B','C2C Pilot','WalletTest3','4PETNEEDS B2B','Infra B2B','6YCOLLECTIVE B2B','DLV Internal Test','PALLETTRACKING B2B','C2CQuikr','DELHIVERYCHEQUES B2B','DARAZBNTEST EXPRESS','AJKERDEALTEST EXPRESS','XBBNTEST EXPRESS','DARAZNPTEST EXPRESS')
      AND pdt not in ('B2B','Heavy')
      

    )
    WHERE ROW = 1
    AND date_cpd >= (date_trunc('day',CURRENT_TIMESTAMP) - interval '1' DAY) - interval '330' MINUTE
    AND date_cpd <= (date_trunc('day',CURRENT_TIMESTAMP) - interval '0' DAY) - interval '330' MINUTE 
      
    group by 1,2,3,4,5
  )
  group by 1,2,3,4,5)
  where Client NOT in ('clt')
  )
  on cnid = facility_code

"""


# In[ ]:


dump_path = "cl_act"
result=run_query_to_athena(query_act, dump_path)


# In[ ]:


local_filename= 'cl_act' + '.csv'
output_act=pd.read_csv(local_filename)


# In[ ]:


output_act.head()


# In[ ]:


from sqlalchemy import create_engine


# In[ ]:


import sqlalchemy


# In[ ]:


engine = create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                              format("shree", "snop321#",
                                                     "datawarehouse-lastmile-analytics-rds.ceypiyhweprx.us-east-1.rds.amazonaws.com", "SNOP"))


# In[ ]:


cnx = engine.raw_connection()


# In[ ]:


output_act.to_sql(name='client_actual_2', con=engine, chunksize=1000, if_exists = 'append', index=False,  dtype={'cpd_date': sqlalchemy.DateTime()})


# In[ ]:


cnx.close()

