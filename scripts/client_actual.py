#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import boto3, csv, time, sys, os, psycopg2,pandas as pd, mysql.connector, re, numpy as np


# In[ ]:


s3 = boto3.resource('s3')


# In[ ]:


from datetime import datetime


# In[ ]:


from datetime import date


# In[ ]:


def run_query(client, query, database, s3_output):
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output,
        }
    )
    # print('Execution ID: ' + response['QueryExecutionId'])
    return response


# In[ ]:


def get_query_status(client, Execution_Id):
    response = client.get_query_execution(
        QueryExecutionId=Execution_Id
    )
    query_id = response['QueryExecution']['QueryExecutionId']
    status = response['QueryExecution']['Status']['State']
    print('Started')
    print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    while status not in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
        # print('|' + status, end='')
        time.sleep(30)
        response = client.get_query_execution(
            QueryExecutionId=Execution_Id
        )
        status = response['QueryExecution']['Status']['State']
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))

    if status in ['SUCCEEDED']:
        return True
    if status in ['FAILED', 'CANCELLED']:
        print("{}: Query ID {} : {}".format(str(datetime.now()), query_id, status))
        print("Failure Reason: {}".format(
            response['QueryExecution']['Status'].get('StateChangeReason', 'Error Unknown')))
        return False
    else:
        return False


# In[ ]:


def run_query_to_athena(query, output_filename):
    client = boto3.client('athena', 'us-east-1' )
    s3_output = "s3://aws-athena-query-results-190020191201-us-east-1/"
    s3_bucket="aws-athena-query-results-190020191201-us-east-1"
    database = 'express_dwh'
    res = run_query(client, query, database, s3_output)

    q_id = res['QueryExecutionId']
    if get_query_status(client, q_id):
        print("Query Successful. Downloading data: ")
        os.system("aws s3 cp " + s3_output + q_id + ".csv  {}".format(output_filename))
        s3_key = q_id + '.csv'
        local_filename = output_filename + '.csv'
        s3.Bucket(s3_bucket).download_file(s3_key, local_filename)

        return True
    else:
        print("Query Unsuccessful. Please check the query or the Athena server status")
        return False


# In[ ]:


query_act = """
select cn,facility_city as city,facility_zone as region,facility_state as state,cpd_date,Client,Cat,act
from
(select facility_code,facility_name,facility_city,replace(facility_type,',','/') facility_type,facility_zone,facility_state
        from
        (
        select "property_facility_facility_code" as facility_code,"property_facility_name" as facility_name,"property_city" as facility_city,
                property_facility_facility_type facility_type,property_zone facility_zone,property_facility_active,property_country,property_state facility_state
                ,row_number() over (partition by "property_facility_facility_code" order by action_date desc) as row
        from facility_ad_json
        ) as t
        where row = 1
        --and property_country = 'India'
        --and facility_type like '%DC%'
        )

join
(
select cn, cnid, cpd_date, Client, 'Actual' cat,sum(box) act from
(select date("date_cpd"+interval '330' minute) cpd_date,cn,cnid,
case when cl in ('Flipkart','FLIPKART - E2E FOOD','FLIPKART E2E','FLIPKART SURFACE') then 'Flipkart'
when cl in ('AMAZONINDIA') then 'Amazon'
when cl in ('SNAPDEAL SURFACE','SNAPDEALRL SURFACE','Snapdeal RL','Snapdeal Air','SNAPDEAL DPP','SNAPDEALXB EXPRESS') then 'Snapdeal'
when cl in ('AJIO SURFACE','AJIO') then 'Ajio'
when cl in ('KARTROCKETSMALL SURFACE','Kart Rocket Surface','Kart Rocket') then 'Kartrocket'
when cl in ('Nykaa E Retail Surface','NYKAA EXPRESS') then 'Nykaa'
when cl in ('FTPL SURFACE','FTPL','FTPL SB','FTPL XB EXPRESS') then 'Ftpl'
when cl in ('PICKRRTECHNOLOGIES','PICKRR SURFACE','PICKRRTECHNOLOGIES BULK','PICKRRTECHNOLOGIES RL','PICKRRTECHNOLOGIES Air','WESHYP SURFACE','PICKKRNEWBULK SURFACE','WESHYPAIR EXPRESS') then 'Pickrr'
when cl in ('MOMOE SC SURFACE','MOMOE SURFACE','MOMOETECH SURFACE','MOMOE EXPRESS','MOMOE SC EXPRESS') then 'Momoe'
when cl in ('Naaptol','NAAPTOL SURFACE') then 'Naaptol'
when cl in ('Myntra') then 'Myntra'
when cl in ('NETMEDS') then 'Netmeds'
when cl in ('REALMEOPPO EXPRESS','REALME SURFACE') then 'Realme'
when cl in ('HANDM SURFACE','HANDM EXPRESS') then 'Handm'
when cl in ('UDAAN','UDAAN SURFACE') then 'Udaan'
when cl in ('SBI CARDS',  'AXISBANK EXPRESS', 'ICICI CARDS EXPRESS',  'TATAUNISTORE', 'PMGINTEGRATED EXPRESS',  'SHOP101COM SURFACE', 'RBLBANK EXPRESS',  'PEACHMODE SURFACE',  'LANDMARKONLINEINDIA',  'IMAGINEMARKETING', 'Decathlon',  'HOPSCOTCH SURFACE',  'SOULEDSTORE SURFACE',  'WESHYP SURFACE', 'ICICI NONSECURED', 'ZARA', 'Bewakoof Surface', 'PHARMEASY',  'ICICI CREDIT CARD',  'Purplle Surface',  'HAMLEYS EXPRESS',  'EZ MALL',  'LENSKART EXPRESS', 'FABINDIA B2C', 'SANYASI SURFACE',  'ShoppersStop', 'FirstCry', 'ICICI BRANCH', 'ORIFLAMEINDIA B2C',  'ALEMBIC SURFACE',  'ZIVAME SURFACE', 'madura', 'MANIPAL EXPRESS',  'USVINDIA SURFACE', 'WESHYPAIR EXPRESS',  'WAKEFIT SURFACE',  'INCNUT EXPRESS', 'SZSTECHIN EXPRESS',  'ADISPORTS',  'YNTSCPL EXPRESS',  'FYNDNDD SURFACE',  'RBL NONSECURED', 'GREENSHOPGO EXPRESS',  'FERNS EXPRESS',  'DECATHLON SURFACE',  'ARVINDINTERNET EXPRESS', 'ARVINDINTERNET', 'DFS - Plumgoodness', 'ARAMEX PU SERVICE',  'VAMASHIP - DFS', 'PUMA EXPRESS', 'LEANBOX SURFACE',  'BYJUS',  'Bata', 'ZARA SURFACE', 'Everstylish',  'HEALTHMUG SURFACE',  'NUTRABAY SURFACE', 'Voylla', 'ARAMEX', 'SELLOSHIP SURFACE',  'MOGLIX SURFACE', 'THINKLEARN SURFACE', 'DFS - FITANDGLOW', 'DCBBANK EXPRESS',  'FCINS EXPRESS',  'JEENA SIKHO LIFE CARE',  'SHIPDESK SURFACE', 'HEALTHKART SURFACE', 'JIVAAYURVEDIC - DFS',  'BAJAJ FINSERV FD', 'WAREIQ SURFACE', 'USPLWORLD EXPRESS',  'CULTGEAR EXPRESS', 'APOLLO EXPRESS', 'DIAPLUSVITA EXPRESS',  'FOREVER21 - COD',  'HERBALIFEINTERNATIO SURFACE',  'AEPL', 'BIWORLDWIDE EXPRESS',  'ZEDLIFESTYLE EXPRESS', 'AJIOFMB2C SURFACE',  'HIMALAYA SURFACE', 'DIGITALYOUTH SURFACE', 'Zivame', 'COOLWINKS - DFS',  'VIVOMOBILE EXPRESS', 'WONDERCHEFMUMBAI SURFACE', 'SPENCERS EXPRESS', 'BESTUNITEDB2C EXPRESS',  'Pepperfry DLH',  'ONGRID EXPRESS', 'TELEONE SURFACE',  'JIMMYS SURFACE', 'AVON', 'MYUPCHAR', 'BIZONGO SURFACE',  'SAVEX SR SMALL', 'THEHUT SURFACE', 'CLICK EXPRESS',  'CR TKAMEEZ', 'SAVEX SURFACE',  'AUSMALL SECURED',  'CUTECODE SSS LLP', 'BEAUTYPEOPLE SURFACE', 'ASHOKLEYLAND SURFACE', 'PAGEINDUSTRIES - DFS', 'Body Shop',  'VEROMODAB2C EXPRESS',  'AXISBANKCREDIT EXPRESS', 'ONLYB2C EXPRESS',  'FCPL Digital', 'ADITYABIRLA EXPRESS',  'OCHRE EXPRESS',  'LYBRATE SURFACE',  'REDCLIFFEHYGIENE SURFACE', 'INDUSIND ADHOC', 'NEWFANGLED SURFACE', 'IFFCO SURFACE') then 'Others1'
else 'Others2' end as "Client"
,"pt",
    (case when ( pdt='' or pdt is null)  then 'B2C' else pdt end) product_typ,
    (case when pdt like '%Heavy%' then 'Heavy' when cl like '%B2B%' then 'B2B' else 'B2C' end) prtyp,zn,
    count(distinct master) vol,
    count(wbn) box
from
(select "date_cpd",date_mnd,"cl","pt",mcount,zn
,(case when date_mnd is null and cl like '%B2B%' then 'B2B' when cl='Flipkart Heavy' then 'Heavy' else "pdt" end) pdt
,cn,cnid,"wbn",master
from 
(select "date_cpd",date_mnd,"cl","pt",mcount,zn,"pdt",cn,cnid,"wbn","mwn",(case when "mwn" is null then wbn else mwn end) master
,row_number() over (partition by "wbn" order by cs_sd desc,action_date desc) as row
from "express_dwh_3m"."package_scan_latest_pq_3m"
where ad > date_format((date_trunc('day',current_timestamp) - interval '03' day) - interval '00' minute, '%Y-%m-%d-%H')
--and "_pt" in ('COD','Pre-paid')
and NOT ("cl" in ('Delhivery','Skynet Logistics Outbound','Skynet Logistics Inbound','FOOD4THOUGHTFOUNDATION B2B','WOLWYNB2B','DEL LS','Quikr Pilot','Delhivery E POD','AIPEXWORLDWIDE B2B','C2C Pilot','WalletTest3','4PETNEEDS B2B','Infra B2B','6YCOLLECTIVE B2B','DLV Internal Test','PALLETTRACKING B2B','C2CQuikr','DELHIVERYCHEQUES B2B','DARAZBNTEST EXPRESS','AJKERDEALTEST EXPRESS','XBBNTEST EXPRESS','DARAZNPTEST EXPRESS','DELHIVERY INTERCHANGE B2B'))
and date_cpd between  (date_trunc('day',current_timestamp + interval '330' minute) - interval '330' minute - interval '1' Day) and (date_trunc('day',current_timestamp + interval '0' day + interval '330' minute) - interval '330' minute)
)
where row=1 
)
group by 1,2,3,4,5,6,7,8)
where prtyp = 'B2C'
group by 1,2,3,4,5
  )
  on cnid = facility_code
"""


# In[ ]:


dump_path = "cl_act"
result=run_query_to_athena(query_act, dump_path)


# In[ ]:


local_filename= 'cl_act' + '.csv'
output_act=pd.read_csv(local_filename)


# In[ ]:


output_act.head()


# In[ ]:


from sqlalchemy import create_engine


# In[ ]:


import sqlalchemy


# In[ ]:


engine = create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                              format("shree", "snop321#",
                                                     "datawarehouse-lastmile-analytics-rds.ceypiyhweprx.us-east-1.rds.amazonaws.com", "SNOP"))


# In[ ]:


cnx = engine.raw_connection()


# In[ ]:


output_act.to_sql(name='client_actual', con=engine, chunksize=1000, if_exists = 'append', index=False,  dtype={'cpd_date': sqlalchemy.DateTime()})


# In[ ]:


cnx.close()

