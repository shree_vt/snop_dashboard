import boto3, csv, time, sys, os, psycopg2,pandas as pd, mysql.connector, re, numpy as np
from datetime import datetime
from datetime import date
from sqlalchemy import create_engine
import sqlalchemy

engine = create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                              format("shree", "snop321#",
                                                     "datawarehouse-lastmile-analytics-rds.ceypiyhweprx.us-east-1.rds.amazonaws.com", "SNOP"))
                                                     
cnx = engine.raw_connection()
output_proj=pd.concat(pd.read_excel('sep_lm_b2c_proj_long.xls', sheet_name=None), ignore_index=True)
output_proj.head()
output_proj.to_sql(name='client_projection', con=engine, chunksize=1000,if_exists = 'append', index=False,  dtype={'cpd_date': sqlalchemy.DateTime()})
cnx.close()
