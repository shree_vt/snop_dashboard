library(shiny)
library(shinydashboard)
library(ggplot2)
library(dplyr)
library(tidyr)
library(DT)
library(reshape2)
library(auth0)
# source("model.R")
# source("ui.R")
# source("server.R")


server <- auth0_server(function(input,output,session){
  # observe(input$dateRange1,{
    # actual_data <- reactive({})
    # actual_data()$cpd_date <- as.Date(actual_data()$cpd_date)
    # colnames(actual_data())[8] <- c('count')
    # proj_data <- reactive({})
    # proj_data()$cpd_date <- as.Date(proj_data()$cpd_date)
    # colnames(proj_data())[8] <- c('count')
    master <- reactive({rbind(actdata_1(input$dateRange1[1],input$dateRange1[2]),projdata_1(input$dateRange1[1],input$dateRange1[2]))}) 
    # })
    master_b2b <- reactive({pricing_tool_invoice(input$month_b2b)})
  
    observeEvent(input$lane_b2b,{
    lane_clients_table <- lane_summary(master_b2b(),input$lane_origin,input$lane_dest)
    overall_lane_table <- overall_lane_summary(master_b2b(),input$lane_origin,input$lane_dest)
    output$lane_clients <- renderDataTable(formatTableDF(lane_clients_table,1))
    output$lane_overall <- renderDataTable(formatTableDF(overall_lane_table,1)) 
    output$download_lane_summary <- downloadHandler(
                                                     filename = function(){
                                                             paste0(input$lane_origin,"_",input$lane_dest,"lane_summary.xlsx")
                                                     },
                                                     content = function(file){
                                                             require(openxlsx)
                                                                list_of_datasets <- list("overall_summary" = overall_lane_table, "client_wise_summary" = lane_clients_table)
                                                                write.xlsx(list_of_datasets, file)
                                                     }
                                                     )
 
})
   url <- a("Click Here", href="https://docs.google.com/spreadsheets/d/1NpvDx1DhosibDJzYhHvc3U9lVRZ-gkcz6EMgRM_Rtp0/edit#gid=165031085",target="_blank") 
    observeEvent(input$client_summary_b2b,{
    b2b_table_pivot <- client_pivot_table(master_b2b(),input$client_b2b)
   b2b_table_summary <- client_summary_table(master_b2b(),input$client_b2b)
   output$cft_box <- renderUI({
     box(width = 3,
      "Client CFT Used: ", client_cft_box(master_b2b(),input$client_b2b),br(),tagList("Client CFT G-Sheet:", url)
    )
   })
   
   output$client_summary <- renderDataTable(formatTableDF(b2b_table_summary,1))
   output$lane_wise_summary <- renderDataTable(formatTableDF(b2b_table_pivot,1))
   output$download_client_summary <- downloadHandler(
						     filename = function(){
							     paste0(input$client_b2b,"_",input$month_b2b,"_summary.xlsx")
						     },
						     content = function(file){
							     require(openxlsx)
						     	    	list_of_datasets <- list("overall_summary" = b2b_table_summary, "lane_wise_summary" = b2b_table_pivot)
								write.xlsx(list_of_datasets, file)
						     }
						     )
  })

  observeEvent(input$overall_summary_b2b,{
   b2b_summary <- overall_summary_table(master_b2b())
   output$overall_b2b_summary <- renderDataTable(formatTableDF(b2b_summary,1))
   top_summary <- business_top_summary(master_b2b(),arr="top")
   bottom_summary <- business_top_summary(master_b2b(),arr="bottom")
   output$bottom_clients <- renderDataTable(formatTableDF(bottom_summary,1))
   output$top_clients <- renderDataTable(formatTableDF(top_summary,1))
   diro_wise <- diro_wise_summary(master_b2b())
   output$diro_summary <- renderDataTable(formatTableDF(diro_wise,1))
   output$download_overall_summary <- downloadHandler(
                                                     filename = function(){
                                                             paste0(input$month_b2b,"_overall_summary.xlsx")
                                                     },
                                                     content = function(file){
                                                             require(openxlsx)
                                                                list_of_datasets <- list("overall_summary" = b2b_summary, "top_50_clients" = top_summary,"bottom_50_clients" = bottom_summary,"director_wise" = diro_wise)
                                                                write.xlsx(list_of_datasets, file)
                                                     }
                                                     )

  })


  observeEvent(input$dc_button,{
    DCTableDf <- proj_act(master(),input$client,input$city)
    output$client_city_data <- renderDataTable(formatTableDF(DCTableDf,1))
    output$client_day_plot <- renderPlotly({city_cl_daywise(master(),input$client,input$city)})
    output$downloadData1 <- downloadHandler(
      filename = function() { 
        paste0(input$city,"_",input$client,"_", input$dateRange1[1],"_",input$dateRange1[2], ".csv", sep="")
      },
      content = function(file) {
        write.csv(DCTableDf, file,row.names = F)
      })
    
  })
  
  observeEvent(input$client_button,{
    DCTableDf5 <- client_summary(master(),input$client1)
    output$client_data <- renderDataTable(formatTableDF(DCTableDf5,1))
    cl_plot <- client_graph(master(),input$client1)
    output$client_plot <- renderPlotly({cl_plot})
    output$downloadData3 <- downloadHandler(
      filename = function() { 
        paste0(input$client1,"_summary",".csv", sep="")
      },
      content = function(file) {
        write.csv(DCTableDf5, file,row.names = F)
      })
  })
  
  observeEvent(input$city3_button,{
    DCTableDf3 <- city3_allcl_summary(master(),input$city3)
    output$city3_data <- renderDataTable(formatTableDF(DCTableDf3,1))
    cl3_plot <- city3_bar_plot(master(),input$city3)
    output$city3_plot <- renderPlotly({cl3_plot})
    DCTableDf_dl <- city3_allcl_download(master(),input$city3)
    output$downloadData2 <- downloadHandler(
      filename = function() { 
        paste0(input$city3,"_all_clients",".csv", sep="")
      },
      content = function(file) {
        write.csv(DCTableDf_dl, file,row.names = F)
      })  
  })
  
  observeEvent(input$city4_button,{
    DCTableDf4 <- raw_data_city(master(),input$city4)
    # DCTableDf5 <- raw_data_state(master(),input$state4)
    # DCTableDf6 <- raw_data_region(master(),input$region4)
    
    output$downloadData4 <- downloadHandler(
      filename = function() { 
        paste0(input$city4,"_raw",".csv", sep="")
      },
      content = function(file) {
        write.csv(DCTableDf4, file,row.names = F)
      })
    
    # output$downloadData5 <- downloadHandler(
    #   filename = function() { 
    #     paste0(input$state4,"_raw",".csv", sep="")
    #   },
    #   content = function(file) {
    #     write.csv(DCTableDf5, file)
    #   })
    # 
    # output$downloadData6 <- downloadHandler(
    #   filename = function() { 
    #     paste0(input$region4,"_raw",".csv", sep="")
    #   },
    #   content = function(file) {
    #     write.csv(DCTableDf6, file)
    #   })
  })
  
  observeEvent(input$city5_button,{
    # DCTableDf4 <- raw_data_city(master(),input$city4)
    DCTableDf5 <- raw_data_state(master(),input$state4)
    # DCTableDf6 <- raw_data_region(master(),input$region4)
    
    # output$downloadData4 <- downloadHandler(
    #   filename = function() { 
    #     paste0(input$city4,"_raw",".csv", sep="")
    #   },
    #   content = function(file) {
    #     write.csv(DCTableDf4, file)
    #   })
    
    output$downloadData5 <- downloadHandler(
      filename = function() { 
        paste0(input$state4,"_raw",".csv", sep="")
      },
      content = function(file) {
        write.csv(DCTableDf5, file,row.names = F)
      })
    
    # output$downloadData6 <- downloadHandler(
    #   filename = function() { 
    #     paste0(input$region4,"_raw",".csv", sep="")
    #   },
    #   content = function(file) {
    #     write.csv(DCTableDf6, file)
    #   })
  })
  
  observeEvent(input$city6_button,{
    # DCTableDf4 <- raw_data_city(master(),input$city4)
    # DCTableDf5 <- raw_data_state(master(),input$state4)
    DCTableDf6 <- raw_data_region(master(),input$region4)
    
    # output$downloadData4 <- downloadHandler(
    #   filename = function() { 
    #     paste0(input$city4,"_raw",".csv", sep="")
    #   },
    #   content = function(file) {
    #     write.csv(DCTableDf4, file)
    #   })
    # 
    # output$downloadData5 <- downloadHandler(
    #   filename = function() { 
    #     paste0(input$state4,"_raw",".csv", sep="")
    #   },
    #   content = function(file) {
    #     write.csv(DCTableDf5, file)
    #   })
    
    output$downloadData6 <- downloadHandler(
      filename = function() { 
        paste0(input$region4,"_raw",".csv", sep="")
      },
      content = function(file) {
        write.csv(DCTableDf6, file,row.names = F)
      })
  })

 observeEvent(input$city7_button,{
    # DCTableDf4 <- raw_data_city(master(),input$city4)
    # DCTableDf5 <- raw_data_state(master(),input$state4)
    DCTableDf7 <- raw_data_client(master())
    
    # output$downloadData4 <- downloadHandler(
    #   filename = function() { 
    #     paste0(input$city4,"_raw",".csv", sep="")
    #   },
    #   content = function(file) {
    #     write.csv(DCTableDf4, file)
    #   })
    # 
    # output$downloadData5 <- downloadHandler(
    #   filename = function() { 
    #     paste0(input$state4,"_raw",".csv", sep="")
    #   },
    #   content = function(file) {
    #     write.csv(DCTableDf5, file)
    #   })
    
    output$downloadData7 <- downloadHandler(
      filename = function() { 
        paste0("client_wise_overall",".csv", sep="")
      },
      content = function(file) {
        write.csv(DCTableDf7, file,row.names = F)
      })
  })
 
  
  })
