library(shiny)
library(shinydashboard)
library(dplyr)
library(tidyr)
library(DT)
library(ggplot2)
library(plotly)
# library(shinycssloaders)
library(shinybusy)
library(auth0)

source("model.R")
# # source("ui.R")
# source("server.R")


# ui <- auth0_ui(function(req){
ui <- function(req){
  
  shinyUI(
    dashboardPage(title = "SNOP Dashboard",
                  dashboardHeader(title = tags$a(href='https://snop.delhivery.com',
                                                 tags$img(src='logo.png',height='20',width='200'))),
                  dashboardSidebar(
                    sidebarMenu(
                      menuItem("Last Mile", tabName = "lm_proj", icon = icon("dashboard")),
                      menuItem("First Mile", tabName = "fm_proj", icon = icon("dashboard"))
                      # #menuItem("LM Productivity Tracker", tabName = "prod_tracker",icon = icon("dashboard")),
                      # menuItem("LM Capacity",tabName = "lm_capacity",icon = icon("dashboard")),
                      # menuItem("OC-CL Tracker",tabName = "oc_cl_tracker", icon = icon("dashboard")),
                      # menuItem("LTL Tracker",tabName = "LTL", icon = icon("dashboard"))
                      
                    )
                  ),
                  dashboardBody(
                    tags$head(tags$style(HTML('
					#cft_box {width:250px;}  
        /* logo */
        .skin-blue .main-header .logo {
                              background-color: #b3b5b4;
                              }
                     /* navbar (rest of the header) */
                    .skin-blue .main-header .navbar {
                              background-color: #cfcfcf;
                              }        '))),
                    tabItems(
                      # tabItem(tabName = "cl_impact",
                      #         fluidPage( fluidRow(
                      #           box(width = 3, status = "primary",
                      #               dateRangeInput('dateRange1',
                      #                              "Select Date Range",
                      #                              start = as.Date("2020-06-15"), end = Sys.Date()-1,
                      #                              min = as.Date("2020-06-15"), max = Sys.Date()-1,
                      #                              startview = 'year', weekstart = 1
                      #               )),
                      #           add_busy_spinner(spin = "radar", margins = c(10, 20))
                      #         ),
                      #         tabBox(width = 12,
                      #                tabPanel("Within City",
                      #                         fluidRow(
                      #                           box(width = 3, status = "primary",
                      #                               selectInput("city", "Choose a city...",
                      #                                           choices =   city)),
                      #                           box(width = 3, status = "primary",
                      #                               selectInput("client", "Choose a client...",
                      #                                           choices = c('All','AZ','FK','CF','OT'))),
                      #                           conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                            actionButton("dc_lm_button","Refresh")),
                      #                           conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                            downloadButton('downloadData1', 'Download after refresh'))),
                      #                         
                      #                         fluidRow(
                      #                           box(width = 4,
                      #                               title = "Data (per day avg)", status = "primary", solidHeader = T,
                      #                               div(style = "overflow-x:scroll", DT::dataTableOutput("client_city_data"))),
                      #                           box(width = 8,
                      #                               title = "Daily Plot", status = "primary", solidHeader = T,div(style = "overflow-x:scroll"),
                      #                               plotlyOutput("client_day_plot"))
                      #                         )),
                      #                tabPanel("Client Summary",
                      #                         fluidRow(
                      #                           box(width = 3, status = "primary",
                      #                               selectInput("client1", "Choose a client...",
                      #                                           choices = c('AZ','FK','CF','OT'))),
                      #                           conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                            actionButton("client_button","Refresh")),
                      #                           conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                            downloadButton('downloadData3', 'Download after refresh'))),
                      #                         
                      #                         fluidRow(
                      #                           box(width = 5,
                      #                               title = "Data (Avg per Day)", status = "primary", solidHeader = T,
                      #                               div(style = "overflow-x:scroll", DT::dataTableOutput("client_data"))),
                      #                           box(width = 7,
                      #                               title = "Most Impacted Cities (Total)", status = "primary", solidHeader = T,
                      #                               plotlyOutput("client_plot"))
                      #                         )),
                      #                
                      #                tabPanel("City Summary",
                      #                         fluidRow(
                      #                           box(width = 3, status = "primary",
                      #                               selectInput("city3", "Choose a city...",
                      #                                           choices = city)),
                      #                           conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                            actionButton("city3_button","Refresh")),
                      #                           conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                            downloadButton('downloadData2', 'Download after refresh'))),
                      #                         
                      #                         fluidRow(
                      #                           box(width = 6,
                      #                               title = "Data", status = "primary", solidHeader = T,
                      #                               div(style = "overflow-x:scroll", DT::dataTableOutput("city3_data"))),
                      #                           box(width = 6,
                      #                               title = "All Clients Impact", status = "primary", solidHeader = T,
                      #                               plotlyOutput("city3_plot"))
                      #                         )),
                      #                tabPanel("Raw Data",
                      #                         fluidRow(
                      #                           box(width = 3, status = "primary",
                      #                               selectInput("city4", "Choose a city...",
                      #                                           choices = city)),
                      #                           conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                            actionButton("city4_button","Refresh")),
                      #                           conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                            downloadButton('downloadData4', 'Download after refresh'))),
                      #                         
                      #                         fluidRow(
                      #                           box(width = 3, status = "primary",
                      #                               selectInput("state4", "Choose a State...",
                      #                                           choices = state)),
                      #                           conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                            actionButton("city5_button","Refresh")),
                      #                           conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                            downloadButton('downloadData5', 'Download after refresh'))),
                      #                         
                      #                         fluidRow(
                      #                           box(width = 3, status = "primary",
                      #                               selectInput("region4", "Choose a Region...",
                      #                                           choices = c('N','S','E','W'))),
                      #                           conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                            actionButton("city6_button","Refresh")),
                      #                           conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                            downloadButton('downloadData6', 'Download after refresh'))),
                      #                         
                      #                         fluidRow(
                      #                           box(width = 3, status = "primary", 
                      #                               conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                                actionButton("city7_button","Refresh for Client Overalls")),
                      #                               conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                                downloadButton('downloadData7', 'Download after refresh'))))
                      #                         
                      #                )))),
                      # tabItem(tabName = "lm_capacity",
                      #         fluidPage(
                      #           tabBox(width = 12,
                      #                  tabPanel("LM Capacity",
                      #                           fluidPage(fluidRow(box(width=3,status = "primary",
                      #                                                  add_busy_spinner(spin = "radar", margins = c(10, 20)),
                      #                                                  conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                                                   actionButton("lm_cap_button","Refresh")),
                      #                                                  conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                                                   downloadButton('download_lm_cap','Download after refresh'))
                      #                                                  
                      #                           )
                      #                           ),
                      #                           fluidRow(box(width = 12,
                      #                                        title = "LM Capacity",status = "primary",solidHeader = T,
                      #                                        div(style = "overflow-x:scroll",DT::dataTableOutput("lm_capacity_table")))
                      #                           ))),
                      #                  tabPanel("ADM Tracker",
                      #                           fluidPage(fluidRow(box(width=3,status = "primary",
                      #                                                  add_busy_spinner(spin = "radar", margins = c(10, 20)),
                      #                                                  conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                                                   actionButton("adm_button","Refresh")),
                      #                                                  conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                                                   downloadButton('download_adm','Download after refresh'))
                      #                                                  
                      #                           )
                      #                           ),
                      #                           fluidRow(box(width = 12,
                      #                                        title = "ADM tracker",status = "primary",solidHeader = T,
                      #                                        div(style = "overflow-x:scroll",DT::dataTableOutput("adm_table")))
                      #                           )) 
                      #                  )
                      #                  
                      #                  
                      #                  
                      #           ))),
                      # tabItem(tabName = "oc_cl_tracker",
                      #         fluidPage(fluidRow(
                      #           box(width=3,status = "primary",
                      #               add_busy_spinner(spin = "radar", margins = c(10, 20)),
                      #               conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                actionButton("fm_oc_cl_button","Refresh")),
                      #               conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                      #                                downloadButton('downloadData_oc_cl','Download after refresh'))
                      #           )
                      #         ),
                      #         
                      #         fluidRow(
                      #           box(width = 12,
                      #               title = "OC-CL Summary",status = "primary", solidHeader = T,
                      #               div(style = "overflow-x:scroll",DT::dataTableOutput("fm_oc_cl_summary"))
                      #               
                      #           )
                      #           
                      #         ),
                      #         fluidRow(
                      #           box(width = 12,
                      #               title = "OC Summary",status = "primary", solidHeader = T,
                      #               div(style = "overflow-x:scroll",DT::dataTableOutput("fm_oc_cl_summary_oc_total"))
                      #               
                      #           )
                      #           
                      #         ),
                      #         fluidRow(
                      #           box(width = 12,
                      #               title = "CL Summary",status = "primary", solidHeader = T,
                      #               div(style = "pverflow-x:scroll",DT::dataTableOutput("fm_oc_cl_summary_cl_total"))
                      #               
                      #           )
                      #           
                      #         )
                      #         
                      #         
                      #         )
                      #         
                      # ),
                      tabItem(tabName = "lm_proj",add_busy_spinner(spin = "radar", margins = c(10, 20)),
                              fluidPage(fluidRow(
                                box(width = 3,status = "primary",
                                    title = "1. Update BD Weekly Projections",
                                    uiOutput("tab_lm1"))
                              ),
                              
                              # fluidRow(
                              #   box(width = 3,status = "primary",
                              #       title = "2. Select Start Date of Projections",
                              #       dateInput("lm_proj_start","Select start date",
                              #                 startview = 'year', weekstart = 1)
                              #                         
                              # )
                              # ),
                              fluidRow(
                                box(width = 3,status = "primary",
                                    title = "2. Populate City Weights G-Sheet",
                                    selectInput("fin_accounts","Select Final Accounts for Projections",choices = client_map$final_account,multiple = T),
                                    add_busy_spinner(spin = "radar", margins = c(10, 20)),
                                    conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                                                     actionButton("lm_hist","Populate"))
                                    
                                )
                              ),
                              fluidRow(
                                box(width = 3,status = "primary",
                                    title = "3. Update City Weights (Update 'use' columns)",
                                    uiOutput("tab_lm2"))
                              ),
                              # fluidRow(
                              #   box(width = 3,status = "primary",
                              #       title = "3. Update daily LM projections",
                              #       uiOutput("tab2"))
                              # ),
                              fluidRow(
                                box(width = 3,status = "primary",
                                    title = "4. Calculate LM Projections",
                                    dateInput("lm_proj_start","Select start date",
                                                              startview = 'year', weekstart = 1),
                                    add_busy_spinner(spin = "radar", margins = c(10, 20)),
                                    conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                                                     actionButton("lm_proj_dist","Calculate"))

                                )
                              ),
                              fluidRow(
                                box(width = 3,status = "primary",
                                    title = "5. Final Output",
                                    uiOutput("tab_lm3"))
                              )
                              
                              )
                              
                      ),
                      tabItem(tabName = "fm_proj",add_busy_spinner(spin = "radar", margins = c(10, 20)),
                              fluidPage(fluidRow(
                                box(width = 3,status = "primary",
                                    title = "1. Populate Historical Trend G-Sheet",
                                                  add_busy_spinner(spin = "radar", margins = c(10, 20)),
                                                  conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                                                                   actionButton("fm_hist","Populate"))
                                                  
                                    )
                              ),
                                fluidRow(
                                box(width = 3,status = "primary",
                                    title = "2. Historical Trend (Update 'use' columns)",
                                    uiOutput("tab1"))
                              ),
                                fluidRow(
                                box(width = 3,status = "primary",
                                    title = "3. Update daily LM projections",
                                    uiOutput("tab2"))
                              ),
                                fluidRow(
                                  box(width = 3,status = "primary",
                                      title = "4. Calculate FM Projections",
                                      add_busy_spinner(spin = "radar", margins = c(10, 20)),
                                      conditionalPanel(condition = "!$('html').hasClass('shiny-busy')",
                                                       actionButton("fm_proj_dist","Calculate"))
                                      
                                  )
                                ),
                              fluidRow(
                                box(width = 3,status = "primary",
                                    title = "5. Final Output",
                                    uiOutput("tab3"))
                              )
                              
                              )

                      )
                      
                      
                    )
                    
                    
                    
                    
                    
                    
                  )))
}
# )
